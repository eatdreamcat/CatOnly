/**
 * 一些工具类方法
 */
export class Utils {

    /** 一年对应的毫秒数 */
    private static readonly YEAR: number = 1000 * 60 * 60 * 24 * 365;
    /** 一个月对应的毫秒数 */
    private static readonly MONTH: number = 1000 * 60 * 60 * 24 * 30;
    /** 一天的毫秒数 */
    private static readonly DAY: number = 1000 * 60 * 60 * 24;
    /** 一个小时的毫秒数 */
    private static readonly HOUR: number = 1000 * 60 * 60;
    /** 一个分钟的毫秒数 */
    private static readonly MIN: number = 1000 * 60;
    /** 一秒的毫秒数 */
    private static readonly SEC: number = 1000;

    /** 格式化时间 */
    private static TimeFormat(year: string, mon: string, day: string, hour: string, min: string, sec: string, format: string): string {
        var o = {
            "y+": year,
            "M+" : mon,
            "d+" : day,
            "h+" : hour,
            "m+" : min,
            "s+" : sec
        };
        for (let k in o) {
            if (new RegExp('(' + k + ')').test(format)) {
                while(RegExp.$1.length > o[k].length && o[k]) {
                    o[k] = '0' + o[k];
                }
                format = format.replace(RegExp.$1, o[k]);
            }
        }
        return format;
    }

    /**
     * 格式化时间 yyyy-MM-dd hh:mm:ss
     * @param time 毫秒
     * @param format 格式yyyy-MM-dd hh:mm:ss
     */
    public static FormatTime(time: number, format: string = 'yyyy-MM-dd hh:mm:ss'): string {
        let date = new Date(time);
        return this.TimeFormat(date.getFullYear().toString(), (date.getMonth() + 1).toString(), 
        date.getDate().toString(), date.getHours().toString(), date.getMinutes().toString(), date.getSeconds().toString(), format);
    }

    /**
     * 格式化计时
     * @param time 毫秒 
     * @param format yyyy-MM-dd hh:mm:ss
     */
    public static FormatCountDown(time: number, format: string = 'yyyy-MM-dd hh:mm:ss') {
        let year = Math.floor(time / this.YEAR);
        time -= (year * this.YEAR);
        let month = Math.floor(time / this.MONTH);
        time -= (month * this.MONTH);
        let day = Math.floor(time / this.DAY);
        time -= (day * this.DAY);
        let hour = Math.floor(time / this.HOUR);
        time -= (hour * this.HOUR);
        let min = Math.floor(time / this.MIN);
        time -= (min * this.MIN);
        let sec = Math.floor(time / this.SEC);

        return this.TimeFormat(year.toString(), month.toString(), day.toString(), hour.toString(), min.toString(), sec.toString(), format);
    }
}