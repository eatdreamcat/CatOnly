import { gSceneMgr } from "../Manager/SceneManager";
import { gNetMgr } from "./Network";
import { CODE, SERVER, MatchType, LayerTag } from "../Define/Define";
import { FightDataExt, gPlayerMgr } from "../Manager/PlayerDataManager";
import { player_baseinfo } from "../Platform/basePlatform";

/** 匹配的数据 */
interface MatchData {
    matchType: MatchType,
    friends: FightDataExt[],
    enemys: FightDataExt[]
}

/**
 * 用来响应服务端返回消息的实例
 */
class Agent {
    private static _ins: Agent;
    public static get inst(): Agent {
        return this._ins ? this._ins : this._ins = new Agent();
    }

    /** 是否有对应的响应接口 */
    public hasMethod(method: string):boolean {
        if (this[method] && typeof this[method] == 'function') {
            return true;
        }
        return false;
    }

    /** 调用对应的接口 */
    public callMethod(method: string, param: any) {
        if (this.hasMethod(method)) {
            this[method](param);
        } else {
            console.warn(' 接口不存在：' + method);
        }
    }

    // ------------------------------------------------ 服务器消息响应 ----------------------------------------
    login(data: player_baseinfo) {
        console.log(' player login -----------------------------');
        console.log(data);
    }

    initBaseInfo(data: player_baseinfo) {
        console.log(' init base info  ---------------------');
        console.log(data);
        gPlayerMgr.initBaseInfo(data);
    }

    /** 被强制下线 */
    beKick() {
        console.log('beKick');
        gSceneMgr.showMsg('已在另一台设备登陆！',()=>{
            cc.game.end();
        });
        gNetMgr.close(SERVER.Logic, CODE.BEKICKED, '被踢下线');
        gNetMgr.close(SERVER.Match, CODE.BEKICKED, '被踢下线');
    }

    /** 匹配玩家成功 */
    onMatchSuccess(param: MatchData) {
        console.log(param);
        gSceneMgr.closePrefabLayer(LayerTag.MatchingBox);
    }

    /** 取消匹配 */
    cancelMatch() {
        gNetMgr.close(SERVER.Match, CODE.COMMON_CLOSE, '关闭匹配');
    }

    /**
     * 被强制取消匹配 
     * 其他玩家掉线，自己会重新进入匹配队列
     */
    forceCancel(param: any) {
        console.log(param);
        if (!param['matchType']) {
            console.error('参数错误');
            gNetMgr.close(SERVER.Match, CODE.COMMON_CLOSE, '关闭匹配');
            return;
        }
        gSceneMgr.showMatchWaiting(param['matchType']);
    }
}

/** 通信消息管理器 */
export const gMsgMgr = Agent.inst;