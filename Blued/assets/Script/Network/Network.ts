import { HashMap } from "../Utils/HashMap";
import { gMsgMgr } from "./Agent";
import { CODE } from "../Define/Define";
import { gEventMgr } from "../Event/EventDispatcher";
import { EVENT } from "../Event/Events";

/** 通信消息的格式 */
interface Message {
    method: string
    data: any
}


/** WebSocket创建的参数 */
interface WebsocketOpt {
    /** 是否自动断线重连 */
    isAutoReconnect: boolean,
    /** 服务器类型 */
    key: number,
    onopen?: (e: Event, key?) => void,
    onclose?: (e: CloseEvent, key?) => void,
    onerror?: (e: Event, key?) => void,
    onmessage?: (e: MessageEvent, key?) => void
}

/** 封装的ws客户端 */
class WebsocketClient {
    client: WebSocket;
    opt: WebsocketOpt = { key: 0 , isAutoReconnect: true};
    url: string;
    private _heartbeat: number = -1;
    public isReconnecting: boolean = false;
    constructor(url: string, key: number, isAutoReconnect: boolean = true) {
        this.client = new WebSocket(url);
        this.opt.key = key;
        this.opt.isAutoReconnect = isAutoReconnect
        this.url = url;
    }

    /** 设置open回调 */
    set onopen(cb: (e: Event, key?) => void) {
        let self = this;
        this.client.onopen = function(e: Event) {
            if (self._heartbeat != -1)  {
                clearInterval(self._heartbeat);
                self._heartbeat = -1;
            }
            // self._heartbeat = setInterval(() => {
            //     self.send(JSON.stringify({ method: "heartbeat", data: Date.now() }));
            // }, 1000);
            cb(e, self.opt.key);
        };

        this.opt.onopen = function(e: Event) {
            if (self._heartbeat != -1)  {
                clearInterval(self._heartbeat);
                self._heartbeat = -1;
            }
            // self._heartbeat = setInterval(() => {
            //     self.send(JSON.stringify({ method: "heartbeat", data: Date.now() }));
            // }, 1000);
            cb(e, self.opt.key);
        };
    }

    /** 设置close回调 */
    set onclose(cb: (e: Event, key?) => void) {
        this.client.onclose = cb;
        this.opt.onclose = cb;
    }
    /** 设置error回调 */
    set onerror(cb: (e: Event, key?) => void) {
        this.client.onerror = cb;
        this.opt.onerror = cb;
    }
    /** 设置message回调 */
    set onmessage(cb: (e: Event, key?) => void) {
        this.client.onmessage = cb;
        this.opt.onmessage = cb;
    }

    /** 获取当前网络状态 */
    get readyState() {
        return this.client.readyState;
    }

    /** 发送数据 */
    send(data) {
        if (this.readyState != WebSocket.OPEN) {
            console.trace(' ws client not connected! key : ' + this.opt.key);
            return;
        }
        console.log(' 客户端发送 消息：' + data);
        this.client.send(data);
    }

    /** 断线重连 TODO:限制一下重连的频率*/
    reconnect() {
        if (!this.opt.isAutoReconnect) return;
        this.isReconnecting = true;
        console.log(' 断线重连：' + this.opt.key);
        if (this._heartbeat != -1) {
            clearInterval(this._heartbeat);
            this._heartbeat = -1;
        }

        this.client.onclose = null;
        this.client.onerror = null;
        this.client.onmessage = null;
        this.client.onopen = null;
        this.client.close(CODE.CONNECT_FAIL);
        this.client = null;
        delete this.client;
        this.client = new WebSocket(this.url);
        this.onclose = this.opt.onclose;
        this.onerror = this.opt.onerror;
        this.onmessage = this.opt.onmessage;
        this.onopen = this.opt.onopen;
    }
}

/**
 * 网络连接单例
 */
class Network {
    private static _ins: Network = null;
    public static get inst(): Network {
        return this._ins ? this._ins : this._ins = new Network();
    }

    /** ws实例 */
    private _wsockets: HashMap<number, WebsocketClient> = new HashMap();
    /** 请求的回调 */
    private _callbacks: HashMap<string, Array<Function>> = new HashMap();

    /** 逻辑服地址 */
    public logicServer: string = '';
    /** 匹配服地址 */
    public matchServer: string = '';

    constructor() {

    }

    /** 连接ws */
    public connect(url: string, opt: WebsocketOpt = { isAutoReconnect: true, key: 0, onopen: (e: Event, key?) => void {}, onclose: (e: CloseEvent, key?) => void {}, onerror: (e: Event, key?) => void {}, onmessage: (e: MessageEvent, key?) => void {} }) {
        if (this._wsockets.has(opt.key)) {
            let client = this._wsockets.get(opt.key);
            if (client.readyState == WebSocket.OPEN) {
                console.warn(' 客户端已连接：' + opt.key);
                return;
            }
        }
        let self = this;
        let wsClient = new WebsocketClient(url, opt.key, opt.isAutoReconnect);
        this._wsockets.add(opt.key, wsClient);

        wsClient.onopen = function (e: Event) {
            console.log(e);
            if (wsClient.isReconnecting) {
                gEventMgr.dispatch(EVENT.RECONNECTED, [opt.key]);
            }
            wsClient.isReconnecting = false;
            opt.onopen && opt.onopen(e, opt.key);
        }.bind(this);

        wsClient.onclose = function (e: CloseEvent) {
            console.log(e);
            opt.onclose && opt.onclose(e, opt.key);
            if (e.code < CODE.CUSTOM) {
                self.reconnect(opt.key);
            }
        }.bind(this);

        wsClient.onmessage = function (e: MessageEvent) {
            console.log(e);
            opt.onmessage && opt.onmessage(e, opt.key);
            console.log('客户端接收消息:' + e.data);
            if (e.data) {
                let data = JSON.parse(e.data) as Message;
                /**
                 * 正常请求
                 * 同一种请求的callback放在同一个key的hashmap里，
                 * 每次该类型的请求响应时，pop出一个callback去执行，因为有可能同一个请求被调用了多次，就会有很多callback
                 */
                if (data.method && self._callbacks.has(data.method)) {
                    let callback = self._callbacks.get(data.method).pop();
                    callback(data);
                    if (self._callbacks.get(data.method).length <= 0) {
                        self._callbacks.remove(data.method);
                    }
                }

                // 有对应的响应接口
                if (gMsgMgr.hasMethod(data.method)) {
                    gMsgMgr.callMethod(data.method, data.data);
                }
            } else {
                console.warn(' 服务端响应了，但是没有返回数据！');
            }
        }.bind(this);

        wsClient.onerror = function (e: Event) {
            console.log(' ================= ws onerror =============== :' + opt.key);
            opt.onerror && opt.onerror(e, opt.key);
            self.reconnect(opt.key);
        }.bind(this);
    }

    /** 发送ws消息 */
    private send(data, key = 0) {
        if (!this._wsockets.has(key)) {
            console.trace(' ws client not exist! key : ' + key);
            return;
        }

        let client = this._wsockets.get(key);
        client.send(data);
    }

    /** 
     * 发送ws请求 
     * 如果是用来做帧同步的，就不需要callback
     * */
    public request(method: string, key: number, data: {}, callback?: Function) {

        if (callback) {
            if (this._callbacks.has(method)) {
                this._callbacks.get(method).push(callback);
                if (this._callbacks.get(method).length > 10) {
                    console.warn(method + ' 请求过于频繁了！！！');
                }
            } else {
                this._callbacks.add(method, [callback]);
            }
        }
        this.send('{"method":"' + method + '","data":' + JSON.stringify(data) + '}', key);
    }

    /** 断线重连 */
    private reconnect(key: number) {
        let client = this._wsockets.get(key);
        if (client) {
            client.reconnect();
        } else {
            console.error(' 当前连接不存在：' + key);
        }
    }


    /** 关闭连接 */
    public close(key: number, code: CODE, reason?: string) {
        if (this._wsockets.has(key)) {
            this._wsockets.get(key).client.close(code, reason);
            this._wsockets.remove(key);
        } else {
            console.error(' 当前连接不存在 ：' + key);
        }
    }

    /** 发送get请求 */
    public httpGet(url: string, data: Object, success: Function, fail?: Function) {
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                success(JSON.parse(xhr.responseText));
            }
        }

        xhr.onerror = function() {
            fail && fail('请求出错！');
        };

        xhr.onabort = function() {
            fail && fail('请求中断!');
        };

        xhr.ontimeout = function() {
            fail && fail('请求超时!');
        }

        let args = '';
        for (let key in data) {
            args += key + "=" + data[key] + '&';
        }
        args = args.substr(0, args.length - 1);
        url = url + "?" + args;
        xhr.open("GET", url, true);
        xhr.send();
    }

    /** 发送post请求 */
    public httpPost(url: string, data: Object, callback: Function) {

    }

}

/** 网络单例 */
export const gNetMgr = Network.inst;