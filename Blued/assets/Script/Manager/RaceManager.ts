import { gPlatformMgr } from "../Platform/PlatfromManager";
import { gNetMgr } from "../Network/Network";
import { gSceneMgr } from "./SceneManager";
import { SERVER, MatchType, CODE, LayerTag } from "../Define/Define";
import { gPlayerMgr } from "./PlayerDataManager";

/** 红蓝方 */
export enum RaceSide {
    Left,
    Right
}


/**
 * 比赛流程管理
 */
class RaceManager {
    private static _ins: RaceManager;
    public static get inst() {
        return this._ins ? this._ins : this._ins = new RaceManager();
    }

    private constructor() { }

    /** 当前匹配的类型 */
    private matchType: MatchType = null;
    /**
     * 开始匹配
     */
    public startMatch(matchType: MatchType) {
        let self = this;
        this.matchType = matchType;
        let loginInfo = gPlatformMgr.getPlatform().getLoginInfo();
        /**
         * 这里流程好复杂， 傻逼了
         */
        gNetMgr.httpGet('http://127.0.0.1:8001/getMatchServer', { uid: loginInfo.uid, os: loginInfo.os }, (res) => {
            if (res.url) {
                gNetMgr.matchServer = res.url;
                gNetMgr.connect(gNetMgr.matchServer, {
                    key: SERVER.Match,
                    isAutoReconnect: false,
                    onclose: function (e: CloseEvent, key) {
                        if (e.code < CODE.CUSTOM) {
                            gSceneMgr.showMsg('匹配连接失败！');
                            gSceneMgr.closePrefabLayer(LayerTag.MatchingBox);
                        }
                    },

                    onmessage: null,

                    onerror: function (e: Event, key) {
                        gSceneMgr.showMsg('连接匹配服失败！');
                        gSceneMgr.closePrefabLayer(LayerTag.MatchingBox);
                    },

                    onopen: function (e: Event, key) {
                        gNetMgr.request('startMatch', SERVER.Match, {uid: gPlayerMgr.uid, matchType: MatchType.Fight_1V1}, self.onJoinMatchSuccess.bind(self));
                    }
                });
            } else {
                gSceneMgr.showMsg('匹配服维护中……');
            }
        }, (errMsg) => {
            gSceneMgr.showMsg(errMsg);
        });
    }

    /** 加入匹配队列成功 */
    private onJoinMatchSuccess(res: {method: string, success: boolean, err: string}) {

        if (res && res.success) {
            gSceneMgr.showMatchWaiting(this.matchType);
        } else {
            gSceneMgr.showMsg("加入匹配队列失败");
        }
    }

}

/** 比赛管理器 */
export const gRaceMgr = RaceManager.inst;