import { player_baseinfo } from "../Platform/basePlatform";

/** 战斗数据 */
export interface FightData {
    uid: number
    /** 排位分 */
    rank: any // { racetype： score}
    /** 连胜数 */
    winStreak: any // { racetype: count}
    /** 最高连胜数 */
    maxWinStreak: any // { racetype: maxcount}
    /** 胜率 */
    winRate: any // {racetype: rate}
    /** 招牌英雄 */
    signatureHero: Array<number>
}

/**
 * 需要返回给客户端的数据
 */
export interface FightDataExt extends FightData {
    /** 昵称 */
    nick: string,
    /** 头像 */
    avatarUrl: string,
    
}


/**
 * 玩家的数据
 */
class PlayerDataManager {
    private static _ins: PlayerDataManager;
    public static get inst() {
        return this._ins ? this._ins : this._ins = new PlayerDataManager();
    }

    private constructor() {}

    private _uid: number = 0;

    /** 初始化玩家基础信息 */
    public initBaseInfo(playerInfo: player_baseinfo) {
        this._uid = playerInfo.uid;
    }

    /** 玩家数字ID */
    public get uid() {
        return this._uid;
    }

}

/** 玩家主要数据管理 */
export const gPlayerMgr = PlayerDataManager.inst;