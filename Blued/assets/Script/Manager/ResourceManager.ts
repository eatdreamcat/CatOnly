const baseURL = 'http://www.jinhualun.com/Queen/';
/**
 * 资源管理器
 * 1.按照场景加载资源，在切换场景的时候，释放旧场景资源并加载新场景资源
 *   因此，不能同时存在两个不同场景资源
 * 
 * 2.界面按照文件夹，打开界面前加载相应文件夹的资源，关闭释放
 *   对于一些经常用到的界面，资源直接常驻缓存
 * 
 * 3.加载界面：界面资源时跟加载场景时界面不一样
 * 
 * 4.资源加载流程，先搜索本地包，没有的话就去CDN加载
 */
class ResourceManager {
    private static _ins: ResourceManager;
    public static get inst() {
        return this._ins ? this._ins : this._ins = new ResourceManager();
    }

    private constructor() {}

    /** 加载本地资源 */
    public loadRes() {

    }

    /** 释放本地资源 */
    public releaseRes() {

    }

    /** 加载远程资源 */
    public loadRemote() {

    }

    /** 释放远程资源 */
    public releaseRemote() {

    }

    /** 加载场景资源 */
    public loadSceneRes(resList, progress, complete) {
        let self = this;
        
    }

    /**
     * 加载文件夹内的资源
     * 有些资源例如图片，会被加载成spriteFrame跟texture2D类型，但是url是一样的
     * 这里把文件夹内的资源按照文件夹名作为key存在_dirRes对象中，每key对应的对象按照url存[]
     * @param dir 文件夹
     * @param progressCallback 过程回调 
     * @param completeCallback 完成回调
     */
    public loadResByDir(dir: string, progressCallback: (completedCount: number, totalCount: number, item: any) => void, completeCallback: () => void) {
        cc.loader.loadResDir(dir, progressCallback, function(error: Error, resource: any[], urls: string[]) {
            if (error) {
                console.error('加载资源错误：' + error);
            } else {
                console.log(resource);
                console.log(urls);
            }
        });
        cc.loader.load(dir, function(err, res) {
            console.log(err);
            console.log(res);
        });
    }

    /**
     * 获取资源
     * @param type 资源类型
     * @param url 资源路径
     */
    public getRes(url: string, type: Function) {

    }
}