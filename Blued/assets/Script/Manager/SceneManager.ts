import { HashMap } from "../Utils/HashMap";
import { LayerTag, MatchType, SERVER } from "../Define/Define";
import { BaseLayer } from "../Views/Base/BaseLayer";
import { gNetMgr } from "../Network/Network";
import { gPlayerMgr } from "./PlayerDataManager";

/** 界面层级 */
export const enum LayerZIndex {
    /** 最底层的基础UI */
    Base = 30,
    /** 弹窗，有交互屏蔽 */
    Pop,
    /** 信息栏， 没有交互屏蔽 */
    Info,
    /** 最顶层 , 有交互屏蔽*/
    Top
}

/**
 * 界面状态信息
 */
interface LayerInfo {
    tag: LayerTag,
    /** 层级 */
    zIndex: LayerZIndex,
    /** 节点挂载的脚本组件 */
    component: BaseLayer,
    /** 节点对象 */
    node: cc.Node,
    /** 对应的预制体名称跟路径 */
    prefab: string,
    /** 资源列表 */
    resList: Array<any>,
    /** 是否打开的标记位, 界面打开需要等加载回调，判断node跟component在瞬间开两次同一个界面时不可靠 */
    opened: boolean
}
/**
 * 场景跟界面管理
 */
class SceneManager {
    private static _ins: SceneManager;
    public static get inst(): SceneManager {
        return this._ins ? this._ins : this._ins = new SceneManager();
    }

    /** 所有注册场景 */
    private _scenes: HashMap<string, boolean> = new HashMap();
    /** 所有注册的预制体界面 */
    private _layers: HashMap<LayerTag, LayerInfo> = new HashMap();
    
    private constructor() {

        // 确认弹窗
        this.registerLayer(LayerTag.PopConfirmLayer, LayerZIndex.Pop, 'prefabs/PopComfirmLayer');
        // 匹配计时窗
        this.registerLayer(LayerTag.MatchingBox, LayerZIndex.Pop, 'prefabs/MatchingBox');
        //账号输入
        this.registerLayer(LayerTag.AccountLayer, LayerZIndex.Base, 'prefabs/AccountLayer');
        //维护界面
        this.registerLayer(LayerTag.ServicingLayer, LayerZIndex.Top, 'prefabs/ServicingLayer');
    }

    /** 注册界面 */
    private registerLayer(tag: LayerTag, zIndex: LayerZIndex, prefab: string, res?: Array<any>) {
        if (this._layers.has(tag)) {
            console.warn('界面已经注册过:' + tag + ',' + prefab);
            return;
        }

        this._layers.add(tag, {
            tag: tag,
            zIndex: zIndex,
            node: null,
            component: null,
            prefab: prefab,
            resList: res,
            opened: false
        });
    }

    /** 获取预制体界面 */
    public getPrefabLayer(tag: LayerTag): BaseLayer {
        if (!this._layers.has(tag)) {
            console.error('界面未注册: ' + tag);
            return null;
        }

        let layer = this._layers.get(tag);
        if (layer.component && layer.node) return layer.component;
        return null;
    }

    /** 显示确认界面 */
    public showMsg(msg: string, ok?: Function, cancel?: Function) {
        let msgLayer = this.getPrefabLayer(LayerTag.PopConfirmLayer);
        console.log(msgLayer);
        if (msgLayer) {
            msgLayer.show(msg, ok, cancel);
        } else {
            this.openPrefabLayer(LayerTag.PopConfirmLayer, msg, ok, cancel);
        }
    }

    /** 显示匹配计时界面 */
    public showMatchWaiting(matchType: MatchType) {
        let matchBox = this.getPrefabLayer(LayerTag.MatchingBox);
        if (matchBox) {
            console.warn(' 匹配窗已打开!');
            return;
        }
        this.openPrefabLayer(LayerTag.MatchingBox, function() {
            gNetMgr.request('cancelMatch', SERVER.Match, {matchType: matchType, uid: gPlayerMgr.uid});
        });
    }

    /** 打开预制体界面 */
    public openPrefabLayer(tag: LayerTag, ...args) {
        if (!this._layers.has(tag)) {
            console.error('界面未注册: ' + tag);
            return;
        }

        let layer = this._layers.get(tag);
        if (layer.component) {
            if (layer.component.isVisiable()) {
                console.log('界面已经打开:' + layer.component.name);
            } else {
                console.log('界面打开:' + layer.component.name);
                layer.component.show(...args);
            }
            return;
        }  
        
        if (layer.opened) {
            console.log('界面正在被打开中……');
            return;
        }

        layer.opened = true;
        cc.loader.loadRes(layer.prefab, cc.Prefab, (err, prefab) => {
            if (err) {
                console.error('预制界面加载失败:' + err);
            } else {
                let prefabLayer = cc.instantiate(prefab) as cc.Node;
                layer.node = prefabLayer;
                let classScript = prefabLayer.getComponent(BaseLayer);
                if (classScript) {
                    layer.component = classScript;
                    let curScene = cc.director.getScene();
                    // 根据层级类型，addChild到当前场景对应的层级根节点
                    let baseNode = curScene.getChildByName('baseLayer' + layer.zIndex);
                    if (!baseNode) {
                        baseNode = new cc.Node();
                        baseNode.name = 'baseLayer' + layer.zIndex;
                        curScene.addChild(baseNode, layer.zIndex);
                        baseNode.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                    }
                    layer.component.show(...args);
                    layer.component.check(layer.zIndex);
                    baseNode.addChild(prefabLayer);
                    console.log('界面打开:' + layer.component.name);
                } else {
                    console.error('找不到界面脚本：' + prefabLayer.name);
                }
            }
        });
    }

    /** 关闭预制界面 */
    public closePrefabLayer(tag: LayerTag) {
        if (!this._layers.has(tag)) {
            console.error('界面未注册: ' + tag);
            return;
        }

        let curScene = cc.director.getScene();
        let layer = this._layers.get(tag);
        if (!layer.component || !layer.node) {
            console.log(layer);
            console.log('界面未初始化：' + tag);
            return;
        }

        let baseNode = curScene.getChildByName('baseLayer' + layer.zIndex);
        if (!baseNode) {
            console.log('未找到层根节点：' + layer.zIndex);
            return;
        }

        if (layer.node.parent != baseNode) {    
            console.error(' 界面挂载层父节点错误!');
        }

        // 这里必须destroy，removeFromParant并不会destroy
        layer.node.removeFromParent(true);
        layer.component.destroy();
        layer.node.destroy();
        layer.node = null;
        layer.component = null;
        layer.opened = false;
    }

    /** 切换场景 */
    changeScene(scene: string, onLaunched?: Function) {
        if (!this._scenes.get(scene)) {
            // TODO 显示loading
        }
        cc.director.loadScene(scene, () => {
            onLaunched && onLaunched();
            // todo 隐藏loading
        });
    }

    /** 预加载场景 */
    preLoadScene(scene: string, onProgress?: (completedCount: number, totalCount: number, item: any) => void, onLoaded?: (error: Error) => void) {
        cc.director.preloadScene(scene, onProgress, (err: Error) => {
            onLoaded && onLoaded(err);
            this._scenes.add(scene, true);
        });
    }
}

/** 场景界面管理 */
export const gSceneMgr = SceneManager.inst;