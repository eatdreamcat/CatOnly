/**
 * 本地缓存管理
 */
class StorageManager {
    private static _ins: StorageManager;
    public static get inst(): StorageManager{
        return this._ins ? this._ins : this._ins = new StorageManager();
    }
    private constructor() {}

    public getItem(key: string) {
        return cc.sys.localStorage.getItem(key);
    }

    public setItem(key: string, value: string) {
        cc.sys.localStorage.setItem(key, value);
    }

    public removeItem(key: string) {
        cc.sys.localStorage.removeItem(key);
    }

    public clear() {
        cc.sys.localStorage.clear();
    }
}

/** 本地缓存管理 */
export const gLocalMgr = StorageManager.inst;