import { Debug } from "./Define";

/** 场景名字 */
export const Scene = {
    Splash: 'SplashScene',
    Main: 'MainScene'
}
