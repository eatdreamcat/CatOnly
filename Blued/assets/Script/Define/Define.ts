export const Debug = true;
/**
 * 碰撞tag
 */
export const enum ColliderTag {
    /** 边界 */
    Border,
    /** 方块 */
    Box
}
/** 服务端返回的状态码,自定义的code必须在3000 - 4999之间 */
export const enum CODE  {

    /** 自定义code的起始 */
    CUSTOM = 2999,
    /** 限制登陆 */
    LOGIN_LIMIT,
    /** 重复登陆，是否踢人 */
    DUPLICATE_LOGIN_KICK,
    /** 被踢下线 */
    BEKICKED,
    /** 连接失败 */
    CONNECT_FAIL,
    /** 正常关闭连接 */
    COMMON_CLOSE
}

/**
 * 匹配的类型
 */
export enum MatchType {
    /** 1v1战斗 */
    Fight_1V1 = 101,
}

/**
 * 服务器的键值
 */
export const enum SERVER {
    /** 逻辑服 */
    Logic,
    /** 匹配服 */
    Match,
    /** 战斗服 */
    Fight
}

/**
 * 界面的定义
 */
export const enum LayerTag {
    /** 匹配计时窗 */
    MatchingBox,
    /** 确认弹窗 */
    PopConfirmLayer,
    /** 账号输入 */
    AccountLayer,
    /** 维护界面 */
    ServicingLayer
}