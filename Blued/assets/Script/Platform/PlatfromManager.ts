import { basePlatform } from "./basePlatform";

/**
 * 平台类型
 */
export enum Platfrom {
    /** 本地账号测试 */
    Account,
    /** 微信 */
    WeChat,
}
/**
 * 平台管理
 */
class PlatformManager {
    private static _ins: PlatformManager;
    public static get inst(): PlatformManager {
        return this._ins ? this._ins : this._ins = new PlatformManager();
    }

    private _platformType: Platfrom = Platfrom.Account;
    private _platform: basePlatform;
    private constructor() {
        if (cc.sys.isBrowser) {
            this._platformType = Platfrom.Account;
            this._platform = new basePlatform();
        }


    }

    /** 获取当前平台接口 */
    public getPlatform(): basePlatform {
        if (!this._platform) {
            this._platform = new basePlatform();
            console.error('平台未初始化！');
        }
        return this._platform;
    }

    /**  */
    public get PlatformType() {
        return this._platformType;
    }

    /**
     * 登陆
     */
    login(complete: (success: boolean, err?: string) => void) {
        if (this._platform) {
            this._platform.login(complete);
        } else {
            console.error('未实例化platform！');
            complete(false, '未实例化platform！');
        }
    }
}

/** 平台管理 */
export const gPlatformMgr = PlatformManager.inst;



