import { gNetMgr } from "../Network/Network";
import { gLocalMgr} from "../Manager/StorageManager";
import { CODE, SERVER } from "../Define/Define";
import { gSceneMgr } from "../Manager/SceneManager";
import { gEventMgr } from "../Event/EventDispatcher";
import { EVENT } from "../Event/Events";
import { gPlayerMgr } from "../Manager/PlayerDataManager";

interface LoginInfo {
    nick: string,
    uid: string,
    openid: string,
    os: string,
}
export interface player_baseinfo {
    nick: string,
    uid: number,
    openid: string,
    os: string,
    register_time: number,
    last_login_time: number
}
/**
 * 平台基础接口
 */
export class basePlatform {

    protected loginInfo: LoginInfo = null;
    constructor() {
        gEventMgr.listen(EVENT.RECONNECTED, this, this.reconnect);
    }

    /** 登陆 */
    login(complete:(success: boolean, err?: string) => void) {
        let self = this;
        gNetMgr.request('login', SERVER.Logic, this.createLoginInfo(), function(res) {
            console.log("登陆回调------------");
            console.log(res);
            if (res.playerInfo) {
                self.loginInfo.uid = res.playerInfo.uid;
                gLocalMgr.setItem('uid', self.loginInfo.uid);
                gPlayerMgr.initBaseInfo(res.playerInfo);
            }
            
            if (res.err == 'OK') {
                complete(true);
            } else {
                if (res.code) {
                    switch(res.code) {
                        case CODE.DUPLICATE_LOGIN_KICK:
                        gSceneMgr.showMsg('已在另一台设备登陆，是否强制登陆?', () => {
                            gNetMgr.request('kick', SERVER.Logic, {
                                uid: self.loginInfo.uid
                            }, function(res) {
                                complete(res.success, res.err);
                            });
                        }, () => {
                            cc.game.end();
                        });
                        break;
                    }
                } else {
                    complete(false, res.err);
                }
            }
        });
    }

    /** 登出 */
    logout() {

    }

    /** 重连 */
    private reconnect(key: number) {
        if (this.loginInfo) {
            gNetMgr.request('reconnected', key, {uid: this.loginInfo.uid}, function(res) {
                console.log(res);
            }.bind(this));
        }
    }

    /**
     * 创建登陆信息
     */
    private createLoginInfo(): LoginInfo {
        if (!this.loginInfo) {
            this.loginInfo = {
                nick: '草泥马',
                uid: gLocalMgr.getItem('uid') || '',
                openid: gLocalMgr.getItem('openid') || '',
                os: cc.sys.os
            }
            if (this.loginInfo.openid == '') {
                this.loginInfo.openid = cc.sys.now().toString();
                gLocalMgr.setItem('openid', this.loginInfo.openid);
            }
        }
        return this.loginInfo;
    }

    /** 设置登陆信息 */
    public setLoginInfo(info: LoginInfo) {
        this.loginInfo = info;
    }

    /** 获取登陆信息 */
    public getLoginInfo(): LoginInfo {
        return this.loginInfo;
    }
}