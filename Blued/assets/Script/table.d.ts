/**
* 导出表自动生成的表数据声明
*/
namespace table {
    /** Sample的e开头导出enum类型，根据批注[类型:值]生成enum定义，并将数据转换成对应的值 */
    export enum Sample_eType{
        /** 第一种 */
        DiYiZhong = 1,
        /** 第二种 */
        DiErZhong = 2,
        /** 第三种 */
        DiSanZhong = 3,
    };

    /** Sample的ae开头，导出enum[]，实际上就是number[] */
    export enum Sample_aeType{
        /** 测试一 */
        CeShiYi = 1,
        /** 测试二 */
        CeShiEr = 2,
        /** 测试三 */
        CeShiSan = 9,
    };




    /** 表 Sample数据结构 */
    export interface ResSample {
        /** Sample的k前缀:导出string类型 */
        kID:string;
        /** Sample的i前缀：导出number类型 */
        iHealth:number;
        /** Sample的ak前缀：会导出string[]类型，以回车分隔每个元素 */
        akIcons:string[];
        /** Sample的e开头导出enum类型，根据批注[类型:值]生成enum定义，并将数据转换成对应的值 */
        eType:number;
        /** Sample的ae开头，导出enum[]，实际上就是number[] */
        aeType:number[];
    };

}