/** 游戏状态 */
export enum GAME_STATE {
    /** 初始化 */
    PREPARE,
    /** 匹配阶段 */
    MATCHING,
    /** 准备完毕 */
    READY,
    /** 开始游戏 */
    START,
    /** 游戏暂停 */
    STOP,
    /** 游戏结束 */
    END
}

/**
 * 单局游戏状态管理 单例
 */
class GameStateMgr {
    private static _inst: GameStateMgr = null;
    public static get inst(): GameStateMgr {
        return this._inst ? this._inst : this._inst = new GameStateMgr();
    }
    private _state: GAME_STATE = GAME_STATE.PREPARE;
    private constructor() {

    }

    public prepare() {
        this._state = GAME_STATE.PREPARE;
    }

    public ready() {
        this._state = GAME_STATE.READY;
    }

    public start() {
        this._state = GAME_STATE.START;
    }

    public stop() {
        this._state = GAME_STATE.STOP;
    }

    public end() {
        this._state = GAME_STATE.END;
    }

    /** 当前游戏的状态 */
    public getState(): GAME_STATE {
        return this._state;
    }

}