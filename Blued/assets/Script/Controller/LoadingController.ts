import { HashMap } from "../Utils/HashMap";
import { gEventMgr } from "../Event/EventDispatcher";
import { EVENT } from "../Event/Events";

/** 加载的步骤 */
export enum LOAD_STEP {
    /** 初始化 */
    READY = 2 << 0,
    /** 连接服务器 */
    CONNECT = 2 << 1,
    /** 用户名输入 */
    ACCOUNT_INIT = 2 << 2,
    /** 加载资源 */
    RESOURCE = 2 << 3,
    /** 解析表数据 */
    JSON_PARSE = 2 << 4,
    /** 完成 */
    DONE = (LOAD_STEP.READY | LOAD_STEP.CONNECT | LOAD_STEP.ACCOUNT_INIT | LOAD_STEP.RESOURCE | LOAD_STEP.JSON_PARSE)
}

class LoadingController {
    private static _ins: LoadingController;
    public static get inst() {
        return this._ins ? this._ins : this._ins = new LoadingController();
    }
    private _step: number = 0;
    private _loadCallbacks: HashMap<LOAD_STEP, Function[]> = new HashMap();
    private constructor() {
        gEventMgr.listen(EVENT.LOAD_NEXT_STEP, this, this.nextStep);
    }

    /** 注册每一步的回调 */
    public registerStep(step: LOAD_STEP, callback: (curStep: LOAD_STEP) => void) {
        if (this._loadCallbacks.has(step)) {
            this._loadCallbacks.get(step).push(callback);
        } else {
            this._loadCallbacks.add(step, [callback]);
        }
    }

    /** 开始加载游戏 */
    public startLoad() {
        this._step |= LOAD_STEP.READY;
        this._loadCallbacks.forEach((step: LOAD_STEP, callbacks: Function[]) => {
            for (let callback of callbacks) {
                callback && callback(step);
            }
        });
    }

    /** 下一步 */
    private nextStep(loadStep: LOAD_STEP) {
        this._step |= loadStep;
        if (this._step >= LOAD_STEP.DONE) {
            gEventMgr.off(EVENT.LOAD_NEXT_STEP, this, this.nextStep);
            gEventMgr.dispatch(EVENT.LOAD_DONE);
        }
    }

}

/** 步骤控制器 */
export const gLoadCtrl = LoadingController.inst;