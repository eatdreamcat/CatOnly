/** 全局事件定义 */
export enum EVENT {
    /** 加载下一步 */
    LOAD_NEXT_STEP,
    /** 初始化完成 */
    LOAD_DONE,
    /** 重连成功 */
    RECONNECTED
}