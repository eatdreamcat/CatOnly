import { HashMap } from "../Utils/HashMap";
import { EVENT } from "./Events";

/** 事件结构 */
interface Event {
    callback: Function,
    caller: any,
    once: boolean
}

/**
 * 事件调度类
 */
class EventDispatcher {
    private static _ins: EventDispatcher;
    public static get inst(): EventDispatcher {
        return this._ins ? this._ins : this._ins = new EventDispatcher();
    }

    /** 事件列表 */
    private _events: HashMap<number, Array<Event>> = new HashMap();
    private _maxEventCounts: number = 300;
    constructor() {

    }

    /**
     * 添加事件
     * @param eventName 事件名，用枚举
     * @param callback 
     * @param once 
     */
    private addEvent(eventName: number, caller: any, callback: Function, once: boolean) {
        if (this._events.has(eventName)) {
            let eventArr: Array<Event> = this._events.get(eventName) as Array<Event>;
            for (let event of eventArr) {
                if (event.callback == callback && caller == event.caller) {
                    console.error(' event already listened:' + eventName);
                    return;
                }
            }
            eventArr.push({
                callback: callback,
                caller: caller,
                once: once
            });
        } else {
            this._events.add(eventName, [{callback: callback, caller: caller, once: once}]);
        }
    }

    /**
     * 监听事件
     * @param eventName 枚举
     * @param callback 
     */
    public listen(eventName: number, caller: any, callback: Function) {
        if (eventName >= this._maxEventCounts) {
            console.warn(' 事件数过多！！！');
        }
        this.addEvent(eventName, caller, callback, false);
    }

    /**
     * 监听单次事件
     * @param eventName 枚举
     * @param callback 
     */
    public listenOnce(eventName: number, caller: any, callback: Function) {
        if (eventName >= this._maxEventCounts) {
            console.warn(' 事件数过多！！！');
        }
        this.addEvent(eventName, caller, callback, true);
    }

    /**
     * 派发事件
     * @param eventName 枚举 
     * @param agr 
     */
    public dispatch(eventName: number, ...agr: any[]) {
        if (this._events.has(eventName)) {
            let eventArr: Array<Event> = this._events.get(eventName) as Array<Event>;
            for (let i = 0; i < eventArr.length; ++i) {
                let event = eventArr[i];
                console.log(' 派发事件：' + EVENT[eventName]);
                console.log(event.caller);
                if (!event.caller) {
                    console.error(' caller invaild!');
                    eventArr.splice(i, 1);
                    --i;
                    continue;
                }
                event.callback.apply(event.caller, ...agr);
                if (event.once) {
                    eventArr.splice(i, 1);
                    --i;
                }
            }
        }
    }

    /** 卸载事件 */
    public off(eventName: number, caller: any, callback: Function) {
        if (this._events.has(eventName)) {
            let eventArr: Array<Event> = this._events.get(eventName) as Array<Event>;
            for (let i = 0; i < eventArr.length; ++i) {
                if (eventArr[i].callback == callback && caller == eventArr[i].caller) {
                    eventArr.splice(i, 1);
                    --i;
                }
            }
        }
    }



}

/** 全局事件管理 */
export const gEventMgr = EventDispatcher.inst;