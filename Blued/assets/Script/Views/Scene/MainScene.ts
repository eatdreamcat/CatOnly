import { gSceneMgr } from "../../Manager/SceneManager";
import { MatchType } from "../../Define/Define";
import { gNetMgr } from "../../Network/Network";
import { gPlatformMgr } from "../../Platform/PlatfromManager";
import { gRaceMgr } from "../../Manager/RaceManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MainScene extends cc.Component {

    /** 比赛模式按钮 */
    @property(cc.Button)
    raceModeButton: cc.Button = null;
    /** 排位模式按钮 */
    @property(cc.Button)
    rankButton: cc.Button = null;
    /** 宫殿按钮 */
    @property(cc.Button)
    palaceButton: cc.Button = null;
    /** 更多按钮 */
    @property(cc.Button)
    moreButton: cc.Button = null;


    onLoad() {
        this.raceModeButton.node.on(cc.Node.EventType.TOUCH_END, ()=>{
            gRaceMgr.startMatch(MatchType.Fight_1V1);
        }, this);

        this.rankButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            gSceneMgr.showMsg('点啥点！');
        }, this);

        this.palaceButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            gSceneMgr.showMsg(null);
        }, this);

        this.moreButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            gSceneMgr.showMsg('我不想写代码');
        }, this);
    }


    start () {

    }

}
