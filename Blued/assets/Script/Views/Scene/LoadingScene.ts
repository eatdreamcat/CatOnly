const { ccclass, property } = cc._decorator;
import { Scene } from "../../Define/SceneDef";
import { gNetMgr } from "../../Network/Network";
import { gLoadCtrl, LOAD_STEP } from "../../Controller/LoadingController";
import { gEventMgr } from "../../Event/EventDispatcher";
import { EVENT } from "../../Event/Events";
import { gSceneMgr } from "../../Manager/SceneManager";
import { gPlatformMgr } from "../../Platform/PlatfromManager";
import { SERVER, LayerTag } from "../../Define/Define";

@ccclass
export default class LoadingScene extends cc.Component {

    @property(cc.ProgressBar)
    /** 进度条 */
    loadProgress: cc.ProgressBar = null;
    @property(cc.Label)
    /** 加载进度文本 */
    loadLabel: cc.Label = null;

    start() {
        this.loadProgress.progress = 0;
        this.loadLabel.string = '加载中...';
        this.registerLoad();
        gLoadCtrl.startLoad();
    }

    /** 注册加载步骤 */
    private registerLoad() {
        // 注册连接服务器步骤
        gLoadCtrl.registerStep(LOAD_STEP.CONNECT, function(step: LOAD_STEP) {
            this.loadProgress.progress = step / LOAD_STEP.DONE;
            this.loadLabel.string = '连接服务器...' + (this.loadProgress.progress * 100).toFixed(2) + '%';
            gNetMgr.httpGet("http://127.0.0.1:8001/getLogicServer",{}, (res) => {
                console.log(res.url);
                gNetMgr.logicServer = res.url;
                gNetMgr.connect(gNetMgr.logicServer, {
                    key: SERVER.Logic,
                    isAutoReconnect: true,
                    onclose: function (e: CloseEvent, key) {
    
                    },
                    onmessage: function (e: MessageEvent, key) {
                        console.log('onmessage');
                        console.log(e);
                        
                    }.bind(this),
                    onerror: function (e: Event, key) {
                        gSceneMgr.openPrefabLayer(LayerTag.ServicingLayer, '服务器维护中...');
                    },
                    onopen: function (e: Event, key) {
                        console.log('onopen');
                        // 下一步
                        gEventMgr.dispatch(EVENT.LOAD_NEXT_STEP, [step]);
                    }
                });
            }, (errMsg)=>{
                gSceneMgr.openPrefabLayer(LayerTag.ServicingLayer, errMsg);
            });
        }.bind(this));

        // 注册加载资源
        gLoadCtrl.registerStep(LOAD_STEP.RESOURCE, function(step: LOAD_STEP) {
            this.loadProgress.progress = step / LOAD_STEP.DONE;
            this.loadLabel.string = '加载资源中...' + (this.loadProgress.progress * 100).toFixed(2) + '%';
            // 下一步
            gSceneMgr.preLoadScene(Scene.Main, null, function(err: Error) {
                if (err) {
                    this.loadLabel.string = '场景加载错误...';
                } else {
                    gEventMgr.dispatch(EVENT.LOAD_NEXT_STEP, [step]);
                }
            }.bind(this));
        }.bind(this));

        // 注册解析json
        gLoadCtrl.registerStep(LOAD_STEP.JSON_PARSE, function(step: LOAD_STEP) {
            this.loadProgress.progress = step / LOAD_STEP.DONE;
            this.loadLabel.string = '解析数据...' + (this.loadProgress.progress * 100).toFixed(2) + '%';
            // 下一步
            setTimeout(function() {
                gEventMgr.dispatch(EVENT.LOAD_NEXT_STEP, [step]);
            }, 100)
        }.bind(this));

        // 加载结束
        gEventMgr.listenOnce(EVENT.LOAD_DONE, this, function() {
            gPlatformMgr.login(function(success, err) {
                console.log('注册登陆:' + success + ':' + err);
                if (success) {
                    gSceneMgr.changeScene(Scene.Main);
                } else {
                    this.loadLabel.string = err;
                }
            });
        }.bind(this));

        // 本地测试用
        gLoadCtrl.registerStep(LOAD_STEP.ACCOUNT_INIT, function(step: LOAD_STEP) {
            this.loadProgress.progress = step / LOAD_STEP.DONE;
            this.loadLabel.string = '初始化账号 ' + (this.loadProgress.progress * 100).toFixed(2) + '%';
            gSceneMgr.openPrefabLayer(LayerTag.AccountLayer, () => {
                gEventMgr.dispatch(EVENT.LOAD_NEXT_STEP, [step]);
            });
        }.bind(this));

    }

    onDestroy() {
        console.log(' LoadingScene onDestroy!');
    }
}



