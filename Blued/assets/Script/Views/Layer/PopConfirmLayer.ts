import { BaseLayer } from "../Base/BaseLayer";
import { gSceneMgr } from "../../Manager/SceneManager";
import { LayerTag } from "../../Define/Define";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
/**
 * 确认弹窗
 */
@ccclass
export default class PopConfirmLayer extends BaseLayer {

    
    @property(cc.Label)
    descLabel: cc.Label = null;
    @property(cc.Button)
    noButton: cc.Button = null;
    @property(cc.Button)
    okButton: cc.Button = null;

    onLoad () {
        console.log(' PopConfirm onLoad!');
    }

    start () {

    }

    isVisiable() {
        return this.node.active;
    }

    /** 注册界面显示内容 */
    private register(desc: string, okCallback: Function, noCallback: Function) {
        if (!noCallback) {
            this.okButton.node.x = 0;
            this.noButton.node.active = false;
        }

        this.okButton.node.off(cc.Node.EventType.TOUCH_END);
        this.okButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            okCallback && okCallback();
            gSceneMgr.closePrefabLayer(LayerTag.PopConfirmLayer);
        }, this);

        this.noButton.node.off(cc.Node.EventType.TOUCH_END);
        this.noButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            noCallback && noCallback();
            gSceneMgr.closePrefabLayer(LayerTag.PopConfirmLayer);
        }, this);

        this.descLabel.string = desc || '把我关掉';
    }

    show(...args) {
        this.register(args[0], args[1], args[2]);
    }
}
