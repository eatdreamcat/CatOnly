import { BaseLayer } from "../Base/BaseLayer";
import { gLocalMgr } from "../../Manager/StorageManager";
import { gSceneMgr } from "../../Manager/SceneManager";
import { LayerTag } from "../../Define/Define";
import { gPlatformMgr } from "../../Platform/PlatfromManager";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
/**
 * 账号登陆界面
 */
@ccclass
export default class AccountLoginLayer extends BaseLayer {

    @property(cc.EditBox)
    inputBox: cc.EditBox = null;
    @property(cc.Button)
    loginButton: cc.Button = null;


    private loginHandler: Function = null;
    onLoad () {
        let name = gLocalMgr.getItem('openid');
        if (name) {
            this.inputBox.placeholder = '';
            this.inputBox.string = name;
        } else {
            this.inputBox.placeholder = '输入用户名...';
            this.inputBox.string = '';
        }
        
        this.inputBox.maxLength = 30;
        this.loginButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            if (this.inputBox.string == '') {
                gSceneMgr.showMsg('用户名不能为空！');
                return;
            }

            if (!/^[a-zA-Z][a-zA-Z0-9_]{4,25}$/.test(this.inputBox.string)) {
                gSceneMgr.showMsg('用户名不合法！');
                return;
            }

            if (this.loginHandler && typeof this.loginHandler == 'function') {
                gPlatformMgr.getPlatform().setLoginInfo({
                    uid: '',
                    nick: this.inputBox.string,
                    openid: this.inputBox.string,
                    os: cc.sys.os
                });
                this.loginHandler();
            }
            gLocalMgr.setItem('openid', this.inputBox.string);
            gSceneMgr.closePrefabLayer(LayerTag.AccountLayer);
        }, this);
    }

    start () {

    }

    isVisiable() {
        return this.node.active;
    }

    show(...args) {
        this.loginHandler = args.shift();
    }
}
