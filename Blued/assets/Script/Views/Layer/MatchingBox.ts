import { BaseLayer } from "../Base/BaseLayer";
import { gSceneMgr } from "../../Manager/SceneManager";
import { LayerTag, SERVER, CODE } from "../../Define/Define";
import { gNetMgr } from "../../Network/Network";
import { Utils } from "../../Utils/Utils";
import { gTimer } from "../../Controller/LoopManager";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
/**
 * 匹配计时窗
 */
@ccclass
export default class MatchingBox extends BaseLayer {

    @property(cc.Button)
    closeButton: cc.Button = null;
    @property(cc.Label)
    timeLabel: cc.Label = null;

    /** 开始匹配的时间 ms*/
    private startMatchTime: number = 0;
    onLoad () {
        this.timeLabel.string = '00:00';
    }

    start () {

    }

    isVisiable() {
        return this.node.active;
    }

    show(...args) {
        let closeCallback = args.shift() as Function;
        this.closeButton.node.on(cc.Node.EventType.TOUCH_END, () => {
            closeCallback && closeCallback();
            gSceneMgr.closePrefabLayer(LayerTag.MatchingBox);
        }, this);

        this.unscheduleAllCallbacks();
        this.startMatchTime = Date.now();
        this.schedule(this.startCountDown, 1);
    }



    /** 开始倒计时 */
    private startCountDown() {
        let matchLastTime = Date.now() - this.startMatchTime;
        this.timeLabel.string = Utils.FormatCountDown(matchLastTime, 'mm:ss');
    }


}
