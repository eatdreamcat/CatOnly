import { LayerZIndex } from "../../Manager/SceneManager";

/**
 * 所有界面的基类
 */
export abstract class BaseLayer extends cc.Component {

    /** 需要屏蔽输入的页面类型 */
    private needBlock = [LayerZIndex.Pop, LayerZIndex.Base, LayerZIndex.Top];
    private needBackground = [LayerZIndex.Pop];

    constructor() {
        super();
        // TODO 这里第一次会被调用两次，具体原因待查
        console.log(' BaseLayer Created!');
    }

    /** 检查一下需不需要加半透输入屏蔽 */
    public check(zIndex: LayerZIndex) {

        // 添加半透背景
        if (this.needBackground.indexOf(zIndex) >= 0 ) {
            let blockBg = this.node.addComponent(cc.Sprite);
            blockBg.sizeMode = cc.Sprite.SizeMode.CUSTOM;
            this.node.width = 1920;
            this.node.height = 1080;
            cc.loader.loadRes("public/single_bg", function(error: Error, tex) {
                if (error) {
                    console.error(error.message);
                } else {
                    blockBg.spriteFrame = new cc.SpriteFrame(tex);
                }
            });
        }

        // 添加输入屏蔽
        if (this.needBlock.indexOf(zIndex) >= 0) {
            this.node.addComponent(cc.BlockInputEvents);
        }
    }

    /** 是否显示状态 */
    abstract isVisiable(): boolean

    /**
     * 显示界面
     * 每个子类用于传参初始化的入口
     * @param args 
     */
    abstract show(...args);

    /** 界面销毁 */
    onDestroy() {
        console.log(' BaseLayer onDestroy!');
    }
}