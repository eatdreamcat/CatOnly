'use strict';
let tableTool = require('./table/index');
module.exports = {
  load() {
   
  },

  unload() {
    
  },

 

  messages: {

    openTableToolPanel() {
      Editor.Panel.open('tabletool-table');
    },

    openTableHelpPanel() {
      Editor.Panel.open('tabletool-help');
    },
    
    startTranTable() {
      tableTool.start();
    }


  }

};