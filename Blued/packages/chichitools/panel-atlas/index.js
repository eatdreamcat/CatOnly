var fs = require('fs');
Editor.Panel.extend({
    style: `
      :host { margin: 5px; }
       h2 { color: #f90; }
    `,

    template: `
      <div>图集导出路径：<ui-input id="jsonPath" placeholder="" readonly = "true"></ui-input><ui-button id="scanJsonPath"  >浏览</ui-button></div>
      <br>
      <div>资源路径：<ui-input id="tablePath" placeholder="" readonly = "true"></ui-input><ui-button id="scanTablePath"  >浏览</ui-button></div>
      <br>
      <ui-progress id="progress" class="small" value="0"></ui-progress>
      <br>
      <ui-button id="btn" class="green" >开始合图</ui-button>
      <hr />
      <div>状态: <span id="label">--</span></div>
    `,

    $: {
        btn: '#btn',
        progress: '#progress',
        label: '#label',
        jsonPath: '#jsonPath',
        scanJsonPath: '#scanJsonPath',
        tablePath: '#tablePath',
        scanTablePath: '#scanTablePath'
    },

    messages: {
        progress(event, progress) {
            this.$progress.value = progress;
            event.reply && event.reply(null, 'OK');
        },

        error(event, errMsg) {
            event.reply && event.reply(null, 'OK');
            if (!errMsg) {
                errMsg = '合图结束';
            }
            this.$label.innerText += '\n' + errMsg;
        }
    },

    config: {},

    ready() {
        this.$jsonPath.value = Editor.url('db://assets/resources/atlas/');
        this.$tablePath.value = Editor.Project.path + '\\Res\\';
        try {
            this.config = JSON.parse(fs.readFileSync(Editor.Project.path  + '\\packages\\chichitools\\config.json')) || {};
        } catch (error) {
            this.$label.innerText = '读取配置文件失败,使用默认配置';
            this.config = {};
        }
        if (this.config['atlas'] && this.config['atlas']['jsonPath']) this.$jsonPath.value = Editor.url(this.config['atlas']['jsonPath']);
        if (this.config['atlas'] && this.config['atlas']['resPath']) this.$tablePath.value = this.config['atlas']['resPath'];

        this.$scanJsonPath.addEventListener('confirm', function() {
            let path = Editor.Dialog.openFile({ 
                defaultPath: Editor.Project.path ,
                properties: [ 'openDirectory' ]
            });
            if (path != -1 && path[0]) {
                if (!this.config['atlas']) this.config['atlas'] = {};
                if (!new RegExp('(' + Editor.Project.name +'\\\\assets)').test(path)) {
                    Editor.Dialog.messageBox({
                        type: 'error',
                        title: '路径选择错误QAQ',
                        message: '图集路径必须在项目的assets下！'
                    });
                    return;
                }
                if (new RegExp('(assets\\\\{0,}[a-zA-z0-9]{0,})').test(path)) {
                    path = RegExp.$1;
                    while(/\\/.test(path)) {
                        path = path.replace(/\\/, '/');
                    }
                  
                    path = "db://" + path + '/';
                } else {
                    Editor.Dialog.messageBox({
                        type: 'error',
                        title: '路径解析错误',
                        message: '可能是我代码出bug了啧啧啧！'
                    });
                    return;
                }
                this.config['atlas']['jsonPath'] = path;
                fs.writeFile(Editor.Project.path  + '\\packages\\chichitools\\config.json', JSON.stringify(this.config), function(err) {
                    if (err) this.$label.innerText = '目录选择失败';
                    else this.$jsonPath.value = Editor.url(path);
                }.bind(this));
            }
        }.bind(this));

        this.$scanTablePath.addEventListener('confirm', function() {
            let path = Editor.Dialog.openFile({ 
                defaultPath: Editor.Project.path ,
                properties: [ 'openDirectory' ]
            });
            if (path != -1 && path[0]) {
                if (!this.config['atlas']) this.config['atlas'] = {};
                this.config['atlas']['resPath'] = path[0];
                fs.writeFile(Editor.Project.path  + '\\packages\\chichitools\\config.json', JSON.stringify(this.config), function(err) {
                    if (err) this.$label.innerText = '目录选择失败';
                    else this.$tablePath.value = path;
                }.bind(this));
            }
        }.bind(this));


        this.$btn.addEventListener('confirm', () => {
            this.$progress.value = 0;
            this.$label.innerText = '\n开始合图';
            Editor.Ipc.sendToPackage('chichitools', 'startMergePng');
        });
    },

});