'use strict';
let tableTool = require('./table/index');
module.exports = {
  load() {
   
  },

  unload() {
    
  },

 

  messages: {

    openTableToolPanel() {
      Editor.Panel.open('chichitools-table');
    },

    openAtlasToolPanel() {
      Editor.Panel.open('chichitools-atlas');
    },

    openHelpPanel() {
      Editor.Panel.open('chichitools-help');
    },
    
    startTranTable() {
      tableTool.start();
    },

    startMergePng() {
      Editor.log('开始合图')
    }


  }

};