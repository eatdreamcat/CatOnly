import express from 'express';
import http from 'http';
import https from 'https';
import ws from 'ws';
import { gMongo, player_baseinfo } from './src/Mongo/mongo';
import { gAgentPool } from './src/Controller/AgentPool';
import { Agent } from './src/agent';
import { UID_BASE, Collections } from './src/Define/Define';
import { gTimer } from './src/Controller/TimerManager';

/** 通信消息的格式 */
interface Message {
    method: string
    data: any
}

let app = express();
let server = new http.Server(app);
//WordCheck.inst.loadSentiveWord('res/table/forbids.ini');

server.listen(8001, function () {
    console.log(' logic server listen on 8001!');
});

let ws_server = new ws.Server({
    port: 8002
}, function () {
    console.log(' ================ logic ws start port:8002 ================== ');
});

/** 限制登陆连接 */
function originIsAllowed() {
    return true;
}

ws_server.on('connection', function (ws: WebSocket) {

    if (!originIsAllowed()) {
        ws.close(4000, 'not allow to connect!');
        return;
    } else {
        console.log(' connect success ！');
        ws.send(JSON.stringify({ data: "connected" }));
    }

    /** 所有请求都走这一步 */
    function onRequest(message: Message, ws: WebSocket) {

        // 心跳
        if (message.method == 'heartbeat') {
            ws.send(JSON.stringify({
                method: message.method,
                success: true,
                err: 'OK'
            }));
            return;
        }

        console.log(message);

        if (!gAgentPool.hasAgent(message.data['uid'])) {
            console.log(' 新建agent:' + message.data['uid']);
            gAgentPool.addAgent(message.data['uid'], new Agent(ws, message.data['uid']));
        }
        let agent = gAgentPool.getAgent(message.data['uid']);
        if (agent[message.method] && typeof agent[message.method] == 'function') {
            agent[message.method](ws, message.data);
        } else {
            console.error(' 接口不存在：' + message.method);
            ws.send(JSON.stringify({
                method: message.method,
                success: false,
                err: '接口不存在'
            }));
        }
    }


    ws.onmessage = function (e: MessageEvent) {
        
        let message: Message = JSON.parse(e.data) as Message;

        /** 如果是登陆操作，判断一下是不是平台，是不是有账号，没有就直接注册一个 */
        if (message.method == 'login') {
            if (message.data['openid'] && message.data['openid'].length) {
                // 来自平台
                gMongo.find(Collections.BaseInfo, { 'openid': message.data['openid'] }).then((arrRes) => {
                    console.log(arrRes);
                    if (arrRes.length == 0) {
                        // 没有注册过
                        gMongo.count(Collections.BaseInfo).then((totalCount: number) => {
                            message.data['uid'] = totalCount + UID_BASE + 1;
                            let player_baseinfo: player_baseinfo = {
                                uid: message.data['uid'],
                                nick: message.data['nick'],
                                openid: message.data['openid'],
                                os: message.data['os'],
                                heros: {},
                                register_time: Date.now(),
                                last_login_time: Date.now()
                            }
                            message.data['register_time'] = player_baseinfo.register_time;
                            message.data['last_login_time'] = player_baseinfo.last_login_time;
                            gMongo.insert(Collections.BaseInfo, [player_baseinfo]).then(() => {
                                console.log('注册成功');
                                onRequest(message, ws);
                            }).catch((reason) => {
                                console.error(' 用户注册失败:' + reason);
                                ws.send(JSON.stringify({
                                    method: 'login',
                                    success: false,
                                    err: '用户注册失败'
                                }));
                            });
                        }).catch((reason) => {
                            console.error(' 查寻用户总数失败:' + reason);
                            ws.send(JSON.stringify({
                                method: 'login',
                                success: false,
                                err: 'uid生成失败'
                            }));
                        });
                    } else {
                        // 注册过的用户
                        message.data['uid'] = arrRes[0]['uid'];
                        gMongo.find(Collections.BaseInfo, { uid: message.data['uid'] }).then((info: player_baseinfo[]) => {
                            console.log('登陆成功');
                            message.data = info[0];
                            onRequest(message, ws);
                        }).catch((reason) => {
                            console.error('登陆失败119：' + reason);
                            ws.send(JSON.stringify({
                                method: 'login',
                                success: false,
                                err: '登陆失败'
                            }));
                        })

                    }
                }).catch((reason) => {
                    console.error(' 数据查找失败：' + reason);
                    ws.send(JSON.stringify({
                        method: 'login',
                        success: false,
                        err: '数据查找失败'
                    }));
                });
            } else {
                // 没有传openid
                if (message.data['uid']) {
                    // TODO 判断一下用户在不在
                } else {
                    // TODO 注册
                }
                ws.send(JSON.stringify({
                    method: 'login',
                    success: false,
                    err: '缺少登陆必要参数openid'
                }));
            }
        } else {
            onRequest(message, ws);
        }


    }

    ws.onclose = function (e: CloseEvent) {
        console.log(' ================== onclose ==================== ');
        console.log(e);
    }

    ws.onerror = function (e: Event) {
        console.log(' ================== onerror ==================== ');
        console.log(e);

    }



});



/***   一些http请求 */
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:7456');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
};
app.use(allowCrossDomain);

// 获取逻辑服地址
app.get('/getLogicServer', function (req, res) {
    res.send(JSON.stringify({
        url: 'ws://127.0.0.1:8002'
    }));
});

// 获取匹配服地址
app.get('/getMatchServer', function (req, res) {
    console.log(req);
    res.send(JSON.stringify({
        url: 'ws://127.0.0.1:9002'
    }));
});