import { HashMap } from '../Utils/HashMap';

/** Timer结构 */
interface TimerLoop {
    /** 调用this环境 */
    caller: any,
    /** 方法 */
    handler: Function,
    /** 时间间隔 */
    delay: number,
    /** 上次调用的时间 */
    lastCallTime: number,
    /** 调用的参数 */
    args: Array<any>,
    /** 总的调用次数 */
    totalTimes: number,
    /** 当前调用次数 */
    curTimes: number,
    /** 是否只执行一次 */
    isOnce: boolean
}

/**
 * timer管理
 */
class LoopManager {
    private static _ins: LoopManager;
    public static get inst() {
        return this._ins ? this._ins : this._ins = new LoopManager();
    }

    /** 是否暂停 */
    private isStop: boolean = false;
    /** 上次调用的时间 */
    private lastCallTime: number = 0;
   
    /** update间隔 毫秒*/
    private readonly DELTTIME: number = 1000 / 60;

    /** 注册的update方法 */
    private intervalHash: HashMap<number, TimerLoop> = new HashMap();
    /** 注册的总数，用来作为ID */
    private intervalTotalCount: number = 0;

    private constructor() {
        console.log(' Timer init!');
        this.start();
    }

    private start() {
        this.isStop = false;
        this.lastCallTime = Date.now();
        setTimeout(this.update.bind(this), this.DELTTIME);
    }

    /**
     * 添加到interval，handler默认第一个参数都是dt
     * @param delay 时间间隔, 毫秒
     * @param caller 调用的this环境
     * @param handler 方法
     * @param args 参数列表
     * @param repeatTimes 重复次数，小于0代表不限制，默认-1
     * @param isOnce 只执行一次,默认false
     */
    public setInterval(delay: number, caller: any, handler: Function, args: Array<any> = [], repeatTimes: number = -1, isOnce: boolean = false): number {
        for (let key of this.intervalHash.keys) {
            let timer = this.intervalHash.get(key);
            if (timer && timer.caller == caller && timer.handler == handler) {
                console.warn(' timer already added: ' + key);
                return key;
            }
        }

        if (delay < this.DELTTIME) {
            console.error(' 时间间隔太短！不得低于一帧的时间！');
            delay = this.DELTTIME;
        }

        ++ this.intervalTotalCount;
        this.intervalHash.add(this.intervalTotalCount, {
            caller: caller,
            handler: handler,
            delay: delay,
            lastCallTime: Date.now(),
            curTimes: 0,
            args: args,
            totalTimes: repeatTimes,
            isOnce: isOnce
        });
        return this.intervalTotalCount;
    }

    /** 真正执行逻辑的地方 */
    private realUpdate(now: number, isJumpFrame: boolean = false) {
        let deleteHashKey = [];
        // 处理各种handler的执行
        for (let key of this.intervalHash.keys) {
            let timer = this.intervalHash.get(key);
            if (!timer) {
                deleteHashKey.push(key);
                console.warn(' timer invaild! ID: ' + key);
                continue;
            }
            if (timer.caller && timer.handler) {

                if (timer.isOnce && timer.curTimes >= 1) {
                    deleteHashKey.push(key);
                    continue;
                }

                if (timer.totalTimes >= 0 && timer.curTimes >= timer.totalTimes) {
                    deleteHashKey.push(key);
                    continue;
                }

                /** 未到达时间间隔 */
                if (timer.lastCallTime + timer.delay > now) continue;

                let args = [timer.delay].concat(timer.args);
                timer.lastCallTime = timer.lastCallTime + timer.delay;
                timer.handler.apply(timer.caller, args);
                ++timer.curTimes;
            } else {
                deleteHashKey.push(key);
                continue;
            }
        }

        //清理需要清楚的updateHandler
        for (let key of deleteHashKey) {
            this.intervalHash.remove(key);
        }
    }

    /** 当前循环的时间间隔 */
    private curDt: number = 0;
    /** 主要update循环 */
    private update() {
        if (this.isStop) return;

        let now = Date.now();
        this.curDt = now - this.lastCallTime;
        this.realUpdate(now);
        this.lastCallTime = now;
        //实际时间间隔大于设定的间隔，提前进入下一个update
        if (this.curDt > this.DELTTIME) {
            //间隔时间超过两倍的delt
            while(2 * this.DELTTIME - this.curDt <= 0) {
                this.realUpdate(now, true);
                this.curDt -= this.DELTTIME;
            }
            setTimeout(this.update.bind(this), 2 * this.DELTTIME - this.curDt);
        } else {
        // 实际用时间比较少，就多等待
            setTimeout(this.update.bind(this), this.DELTTIME + this.DELTTIME - this.curDt);
        }
    }
}

/**
 * 调度管理器
 */
export const gTimer = LoopManager.inst;