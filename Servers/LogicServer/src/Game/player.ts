import { gMongo, player_baseinfo } from '../Mongo/mongo';
import { Collections } from "../Define/Define";

/**
 * 玩家数据实例
 */
export class Player {
    private _playerInfo: player_baseinfo = {
        uid: 0,
        nick: '',
        openid: '',
        heros: {},
        os: '',
        register_time: 0,
        last_login_time: 0
    };

    /** 是否初始化玩家信息 */
    public isInit: boolean = false;
    constructor(uid: number) {
        this._playerInfo.uid = uid;
    }

    public get uid() {
        return this._playerInfo.uid;
    }

    /** 获取用户的基础信息 */
    public get playerInfo() {
        return this._playerInfo;
    }

    /** 设置玩家基础信息 */
    public setPlayerInfo(info: player_baseinfo) {
        this._playerInfo = info;
        this.isInit = true;
    }

    /** 把数据更新到db */
    public saveToDb() {
        gMongo.update(Collections.BaseInfo, this._playerInfo, { uid: this.uid}).then(() => {
            console.log(' 更新player_baseinfo 成功');
        }).catch((err) => {
            console.log(' 更新数据失败:' + err);
        });
    }
}