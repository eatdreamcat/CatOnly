"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 连接mongoDB用
 */
const mongodb_1 = __importDefault(require("mongodb"));
let url = "mongodb://localhost:27017/";
let user = "chichi";
let password = "mango7";
let dbName = "blued";
/** mongo管理 */
class Mongo {
    static get inst() {
        return this._ins ? this._ins : this._ins = new Mongo();
    }
    constructor() {
    }
    /** 连接db */
    _connect() {
        return new Promise(function (resolve, reject) {
            mongodb_1.default.MongoClient.connect(url, {
                useNewUrlParser: false,
            }, function (err, db) {
                if (err) {
                    console.error(' connect mongo error: ' + err);
                    reject(err);
                }
                else {
                    console.log(' connect to mongo success !');
                    resolve(db);
                }
            });
        });
    }
    /**
     * 插入数据
     * @param collection 表名
     * @param data 数据集合
     */
    insert(collection, data) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self._connect().then((dbClient) => {
                dbClient.db(dbName).collection(collection).insertMany(data, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(res);
                    }
                    dbClient.close();
                });
            }).catch((reason) => {
                reject(reason);
            });
        });
    }
    /**
     * 查询数据
     * @param collection 表名
     * @param where 查询条件，例如{“uid”: 2000}
     * @param sort 按字段排序, {sortType: 1 升序，-1 降序}
     */
    find(collection, where = {}, sort = {}) {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).find(where).sort(sort).toArray();
        });
    }
    /**
     * 查询记录条数
     * @param collection
     */
    count(collection) {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).find().count();
        });
    }
    /**
     * 更新数据
     * @param collection 表名
     * @param updateVal 更新的数据集合
     * @param where 查询条件
     */
    update(collection, updateVal, where = {}) {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).updateMany(where, { $set: updateVal });
        });
    }
    /** 删除数据 */
    delete(collection, where = {}) {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).deleteMany(where);
        });
    }
}
/** 数据库 */
exports.gMongo = Mongo.inst;
//# sourceMappingURL=mongo.js.map