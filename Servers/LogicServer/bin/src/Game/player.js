"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo_1 = require("../Mongo/mongo");
const Define_1 = require("../Define/Define");
/**
 * 玩家数据实例
 */
class Player {
    constructor(uid) {
        this._playerInfo = {
            uid: 0,
            nick: '',
            openid: '',
            heros: {},
            os: '',
            register_time: 0,
            last_login_time: 0
        };
        /** 是否初始化玩家信息 */
        this.isInit = false;
        this._playerInfo.uid = uid;
    }
    get uid() {
        return this._playerInfo.uid;
    }
    /** 获取用户的基础信息 */
    get playerInfo() {
        return this._playerInfo;
    }
    /** 设置玩家基础信息 */
    setPlayerInfo(info) {
        this._playerInfo = info;
        this.isInit = true;
    }
    /** 把数据更新到db */
    saveToDb() {
        mongo_1.gMongo.update(Define_1.Collections.BaseInfo, this._playerInfo, { uid: this.uid }).then(() => {
            console.log(' 更新player_baseinfo 成功');
        }).catch((err) => {
            console.log(' 更新数据失败:' + err);
        });
    }
}
exports.Player = Player;
//# sourceMappingURL=player.js.map