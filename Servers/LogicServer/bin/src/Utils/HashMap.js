"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * HashMap泛型实现
 */
class HashMap {
    constructor() {
        //存储列表
        this._list = new Array();
        this.clear();
    }
    //通过key获取索引
    getIndexByKey(key) {
        var count = this._list.length;
        for (let index = 0; index < count; index++) {
            const element = this._list[index];
            if (element.key == key) {
                return index;
            }
        }
        return -1;
    }
    /** 获取所有key */
    get keys() {
        let keys = new Array();
        for (let element of this._list) {
            if (element) {
                keys.push(element.key);
            }
        }
        return keys;
    }
    /**
     * 添加键值
     */
    add(key, value) {
        var data = { key: key, value: value };
        var index = this.getIndexByKey(key);
        if (index != -1) {
            //已存在：刷新值
            this._list[index] = data;
        }
        else {
            //不存在：添加值
            this._list.push(data);
        }
    }
    /**
     * 删除键值
     */
    remove(key) {
        var index = this.getIndexByKey(key);
        if (index != -1) {
            var data = this._list[index];
            this._list.splice(index, 1);
            return data;
        }
        return null;
    }
    /**
     * 是否存在键
     */
    has(key) {
        var index = this.getIndexByKey(key);
        return index != -1;
    }
    /**
     * 通过key获取键值value
     * @param key
     */
    get(key) {
        var index = this.getIndexByKey(key);
        if (index != -1) {
            var data = this._list[index];
            return data.value;
        }
        return null;
    }
    /**
     * 获取数据个数
     */
    get length() {
        return this._list.length;
    }
    /**
     * 排序
     * @param
     */
    sort(compare) {
        this._list.sort(compare);
    }
    /**
     * 遍历列表，回调(data:KeyValue<K, V>)
     */
    forEachKeyValue(f) {
        var count = this._list.length;
        for (let index = 0; index < count; index++) {
            const element = this._list[index];
            f(element);
        }
    }
    /**
     * 遍历列表，回调(K,V)
     */
    forEach(f) {
        var count = this._list.length;
        for (let index = 0; index < count; index++) {
            const element = this._list[index];
            f(element.key, element.value);
        }
    }
    /**
     * 清空全部
     */
    clear() {
        this._list = [];
    }
}
exports.HashMap = HashMap;
//# sourceMappingURL=HashMap.js.map