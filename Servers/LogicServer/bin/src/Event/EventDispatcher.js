"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HashMap_1 = require("../Utils/HashMap");
/**
 * 事件调度类
 */
class EventDispatcher {
    constructor() {
        /** 事件列表 */
        this._events = new HashMap_1.HashMap();
        this._maxEventCounts = 300;
    }
    static get inst() {
        return this._ins ? this._ins : this._ins = new EventDispatcher();
    }
    /**
     * 添加事件
     * @param eventName 事件名，用枚举
     * @param callback
     * @param once
     */
    addEvent(eventName, caller, callback, once) {
        if (this._events.has(eventName)) {
            let eventArr = this._events.get(eventName);
            for (let event of eventArr) {
                if (event.callback == callback) {
                    console.error(' event already listened:' + eventName);
                    return;
                }
            }
            eventArr.push({
                callback: callback,
                caller: caller,
                once: once
            });
        }
    }
    /**
     * 监听事件
     * @param eventName 枚举
     * @param callback
     */
    listen(eventName, caller, callback) {
        if (eventName >= this._maxEventCounts) {
            console.warn(' 事件数过多！！！');
        }
        this.addEvent(eventName, caller, callback, false);
    }
    /**
     * 监听单次事件
     * @param eventName 枚举
     * @param callback
     */
    listenOnce(eventName, caller, callback) {
        if (eventName >= this._maxEventCounts) {
            console.warn(' 事件数过多！！！');
        }
        this.addEvent(eventName, caller, callback, true);
    }
    /**
     * 派发事件
     * @param eventName 枚举
     * @param agr
     */
    dispatch(eventName, ...agr) {
        if (this._events.has(eventName)) {
            let eventArr = this._events.get(eventName);
            for (let i = 0; i < eventArr.length; ++i) {
                let event = eventArr[i];
                console.log(' 派发事件：' + eventName);
                console.log(event.caller);
                if (!event.caller) {
                    console.error(' caller invaild!');
                    eventArr.splice(i, 1);
                    --i;
                    continue;
                }
                event.callback.apply(event.caller, ...agr);
                if (event.once) {
                    eventArr.splice(i, 1);
                    --i;
                }
            }
        }
    }
    /** 卸载事件 */
    off(eventName, caller, callback) {
        if (this._events.has(eventName)) {
            let eventArr = this._events.get(eventName);
            for (let i = 0; i < eventArr.length; ++i) {
                if (eventArr[i].callback == callback && caller == eventArr[i].caller) {
                    eventArr.splice(i, 1);
                    --i;
                }
            }
        }
    }
}
//# sourceMappingURL=EventDispatcher.js.map