"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HashMap_1 = require("../Utils/HashMap");
/**
 * 管理所有agent实例
 */
class AgentPool {
    constructor() {
        /** 所有agent缓存池 */
        this._agents = new HashMap_1.HashMap();
        /** 频道,每个频道对应一个HashMap */
        this._channels = new HashMap_1.HashMap();
    }
    static get inst() {
        return this._ins ? this._ins : this._ins = new AgentPool();
    }
    /** 添加agent */
    addAgent(id, agent) {
        if (this._agents.has(id)) {
            console.error(' agent already exists : ' + id);
            return;
        }
        this._agents.add(id, agent);
        this.addToChannel(0 /* DEFAULT */, id, agent);
    }
    /** 删除agent */
    removeAgent(id, removeFromChannel = true) {
        if (this._agents.has(id)) {
            this._agents.remove(id);
            if (removeFromChannel) {
                this.removeAgentFromChannel(0 /* DEFAULT */, id);
            }
        }
    }
    /** 清理频道内的agent引用 */
    removeAgentFromChannel(channel, id) {
        if (this._channels.has(channel)) {
            let agentHash = this._channels.get(channel);
            if (agentHash.has(id)) {
                agentHash.remove(id);
            }
        }
    }
    /** 踢出频道 */
    kickFromChannel(channel, id, isClear = false) {
        this.removeAgentFromChannel(channel, id);
        if (isClear) {
            this.removeAgent(id, false);
        }
    }
    /** 加入频道 */
    addToChannel(channel, id, agent) {
        if (!this._channels.has(channel)) {
            this._channels.add(channel, new HashMap_1.HashMap());
        }
        let agentHash = this._channels.get(channel);
        if (agentHash.has(id)) {
            console.error(' agent already exists : ' + id + ', channel:' + channel);
            return;
        }
        agentHash.add(id, agent);
    }
    /** 是否存在agent */
    hasAgent(id) {
        return this._agents.has(id);
    }
    /** 获取agent */
    getAgent(uid) {
        return this._agents.get(uid);
    }
    /** 按照频道广播 */
    broadcast(channel, message) {
        if (this._channels.has(channel)) {
            let agentHash = this._channels.get(channel);
            agentHash.forEach((id, agent) => {
                // todo 发送数据
            });
        }
        else {
            console.error('频道不存在：' + channel);
        }
    }
}
exports.AgentPool = AgentPool;
//# sourceMappingURL=AgentPool.js.map