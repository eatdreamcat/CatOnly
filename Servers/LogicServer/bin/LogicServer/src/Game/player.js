"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo_1 = require("../Mongo/mongo");
const Define_1 = require("../../../Define/Define");
/**
 * 玩家数据实例
 */
class Player {
    constructor(uid) {
        this._playerInfo = {
            uid: 0,
            nick: '',
            openid: '',
            os: '',
            register_time: 0,
            last_login_time: 0
        };
        this._playerInfo.uid = uid;
    }
    get uid() {
        return this._playerInfo.uid;
    }
    /** 设置玩家基础信息 */
    setPlayerInfo(info) {
        this._playerInfo = info;
    }
    /** 把数据更新到db */
    saveToDb() {
        mongo_1.Mongo.inst.update(Define_1.Collections.BaseInfo, this._playerInfo, { uid: this.uid }).then(() => {
            console.log(' 更新player_baseinfo 成功');
        }).catch((err) => {
            console.log(' 更新数据失败:' + err);
        });
    }
}
exports.Player = Player;
//# sourceMappingURL=player.js.map