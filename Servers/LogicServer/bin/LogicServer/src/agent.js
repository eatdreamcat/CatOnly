"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const player_1 = require("./Game/player");
/**
 * 前后端通信的实例
 * 每一个玩家连接都对应一个agent对象
 */
class Agent {
    constructor(ws, uid) {
        /** ws 引用 */
        this._client = null;
        this._player = null;
        this._isOnline = false;
        this._isOnRace = false;
        this._client = ws;
        this._player = new player_1.Player(uid);
        this._client.onclose = this.onClose.bind(this);
        this._client.onerror = this.onError.bind(this);
    }
    /** 登陆 */
    login(ws, playInfo) {
        console.log(' agent login ');
        console.log(playInfo);
        playInfo.last_login_time = Date.now();
        this._player.setPlayerInfo(playInfo);
        if (this._isOnline) {
            // 在线状态重复登陆了
            if (this._isOnRace) {
                // 如果在比赛中，就不给强制挤人
                ws.send(JSON.stringify({
                    method: 'login',
                    success: false,
                    err: '比赛中，无法登陆'
                }));
            }
            else {
                // 不在比赛中，让客户端决定是否踢人
                ws.send(JSON.stringify({
                    method: 'login',
                    success: true,
                    err: 'Duplicate',
                    playerInfo: playInfo,
                    code: 3001 /* DUPLICATE_LOGIN_KICK */
                }));
            }
        }
        else {
            this._isOnline = true;
            this.client = ws;
            this._client.send(JSON.stringify({
                method: 'login',
                success: true,
                err: 'OK',
                playerInfo: playInfo
            }));
        }
    }
    /** 心跳 */
    heartbeat() {
    }
    /**
     * 重复登陆，把旧的client踢掉
     * @param ws 新的ws，把旧的踢掉
     */
    kick(ws) {
        /** 旧客户端通知被踢 */
        this.client.send(JSON.stringify({
            method: 'beKick',
        }));
        /** 通知新客户端踢人成功 */
        this.client = ws;
        this.client.send(JSON.stringify({
            method: 'kick',
            success: true,
            err: 'OK'
        }));
    }
    /** 清理agent */
    onClose(e) {
        console.log(' onClose -----------------');
        console.log(e);
        if (e.code == 3002 /* BEKICKED */) {
            // 被踢下线，本质上只是换个设备在线，所以不需要设置下线
            // 将旧的客户端监听绑定到新的
            this._client.onclose = this.onClose.bind(this);
            this._client.onerror = this.onError.bind(this);
        }
        else {
            this._isOnline = false;
            console.log('玩家下线：' + this._player.uid);
        }
        // AgentPool.inst.removeAgent(this._player.uid);
    }
    onError(e) {
        console.log('on error -----');
        console.log(e);
    }
    /** 获取当前的websocket */
    get client() {
        return this._client;
    }
    /** 设置当前的websocket */
    set client(cli) {
        this._client = cli;
    }
}
exports.Agent = Agent;
//# sourceMappingURL=agent.js.map