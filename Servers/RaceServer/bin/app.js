"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const ws_1 = __importDefault(require("ws"));
const mongo_1 = require("./src/Mongo/mongo");
const AgentPool_1 = require("./src/Controller/AgentPool");
const agent_1 = require("./src/agent");
const WordCheck_1 = require("./src/Utils/WordCheck");
const proto_1 = require("./src/proto/proto");
const Define_1 = require("./src/Define/Define");
let app = express_1.default();
let server = new http_1.default.Server(app);
WordCheck_1.WordCheck.inst.loadSentiveWord('res/table/forbids.ini');
server.listen(8001, function () {
    console.log(' server listen on 8001!');
});
let ws_server = new ws_1.default.Server({
    port: 8002
}, function () {
    console.log(' ================ ws start port:8002 ================== ');
});
/** 限制登陆连接 */
function originIsAllowed() {
    return true;
}
ws_server.on('connection', function (ws) {
    if (!originIsAllowed()) {
        ws.close(4000, 'not allow to connect!');
        return;
    }
    else {
        console.log(' connect success ！');
        ws.send(JSON.stringify({ data: "connected" }));
    }
    function onRequest(message) {
        if (!AgentPool_1.AgentPool.inst.hasAgent(message.data['uid'])) {
            AgentPool_1.AgentPool.inst.addAgent(message.data['uid'], new agent_1.Agent(ws, message.data['uid']));
        }
        let agent = AgentPool_1.AgentPool.inst.getAgent(message.data['uid']);
        if (agent[message.method] && typeof agent[message.method] == 'function') {
            agent[message.method](message.data);
        }
        else {
            console.error(' 接口不存在：' + message.method);
        }
    }
    ws.onmessage = function (e) {
        console.log(' ================== onmessage =================== ');
        let message = JSON.parse(e.data);
        /** 如果是登陆操作，判断一下是不是平台，是不是有账号，没有就直接注册一个 */
        if (message.method == proto_1.c2s.login) {
            if (message.data['openid'] && message.data['openid'].length) {
                // 来自平台
                mongo_1.Mongo.inst.find('player_baseinfo', { 'openid': message.data['openid'] }).then((arrRes) => {
                    if (arrRes.length == 0) {
                        // 没有注册过
                        mongo_1.Mongo.inst.count('player_baseinfo').then((totalCount) => {
                            message.data['uid'] = totalCount + Define_1.UID_BASE;
                            mongo_1.Mongo.inst.insert('player_baseinfo', [{
                                    uid: message.data['uid'],
                                    openid: message.data['openid']
                                }]).then(() => {
                                console.log('注册成功');
                                onRequest(message);
                            }).catch((reason) => {
                                console.error(' 用户注册失败:' + reason);
                            });
                        }).catch((reason) => {
                            console.error(' 查寻用户总数失败:' + reason);
                        });
                    }
                    else {
                        // 注册过的用户
                        message.data['uid'] = arrRes[0]['uid'];
                        onRequest(message);
                    }
                }).catch((reason) => {
                    console.error(' 数据查找失败：' + reason);
                });
            }
            else {
                // 没有传openid
                if (message.data['uid']) {
                    // TODO 判断一下用户在不在
                }
                else {
                    // TODO 注册
                }
            }
        }
        else {
            onRequest(message);
        }
    };
    ws.onclose = function (e) {
        console.log(' ================== onclose ==================== ');
        console.log(e);
    };
    ws.onerror = function (e) {
        console.log(' ================== onerror ==================== ');
        console.log(e);
    };
});
/***   一些http请求 */
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:7456');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
};
app.use(allowCrossDomain);
// 获取逻辑服地址
app.get('/getLogicServer', function (req, res) {
    res.send(JSON.stringify({
        url: 'ws://127.0.0.1:8002'
    }));
});
//# sourceMappingURL=app.js.map