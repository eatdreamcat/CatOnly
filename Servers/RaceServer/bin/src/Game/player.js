"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 玩家数据实例
 */
class Player {
    constructor(uid) {
        this._playerInfo = {
            uid: -1,
            nick: '',
            openid: '',
            platform: '',
        };
        this._playerInfo.uid = uid;
    }
    get uid() {
        return this._playerInfo.uid;
    }
}
exports.Player = Player;
//# sourceMappingURL=player.js.map