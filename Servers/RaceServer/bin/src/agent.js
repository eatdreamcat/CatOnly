"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const player_1 = require("./Game/player");
/**
 * 前后端通信的实例
 * 每一个玩家连接都对应一个agent对象
 */
class Agent {
    constructor(ws, uid) {
        /** ws 引用 */
        this._client = null;
        this._player = null;
        this._client = ws;
        this._player = new player_1.Player(uid);
    }
    /** 登陆 */
    login() {
    }
    /** 心跳 */
    heartbeat() {
    }
    /** 清理agent */
    destroy() {
        console.log('清理agent：' + this._player.uid);
    }
}
exports.Agent = Agent;
//# sourceMappingURL=agent.js.map