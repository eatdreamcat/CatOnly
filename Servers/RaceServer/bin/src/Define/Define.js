"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** 服务端返回的状态码 */
exports.CODE = {
    /** 限制登陆 */
    LOGIN_LIMIT: 4000
};
/** uid基数 */
exports.UID_BASE = 50000;
//# sourceMappingURL=Define.js.map