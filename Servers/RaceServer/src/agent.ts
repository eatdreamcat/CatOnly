import { Player } from "./Game/player";

/**
 * 前后端通信的实例
 * 每一个玩家连接都对应一个agent对象
 */
export class Agent {
    /** ws 引用 */
    private _client: WebSocket = null;
    private _player: Player = null;
    constructor(ws: WebSocket, uid: number) {
        this._client = ws;
        this._player = new Player(uid);
    }

    /** 登陆 */
    login() {
        
    }

    /** 心跳 */
    heartbeat() {

    }

    /** 清理agent */
    destroy() {
        console.log('清理agent：' + this._player.uid)
    }
}