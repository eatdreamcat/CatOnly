import { platform } from "os";

export interface player_info {
    uid: number,
    nick: string,
    openid: string, 
    platform: string
}
/**
 * 玩家数据实例
 */
export class Player {
    private _playerInfo: player_info = {
        uid: -1,
        nick: '',
        openid: '',
        platform: '',
    };

    constructor(uid: number) {
        this._playerInfo.uid = uid;
    }

    public get uid() {
        return this._playerInfo.uid;
    }
}