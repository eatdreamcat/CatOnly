/**
 * 客户端向服务端通讯
 */
export const c2s = {
    login: 'login'
}

/**
 * 服务端向客户端通信
 */
export const s2c = {
    login: 'login'
}