import { Agent } from "../agent";
import { HashMap } from "../Utils/HashMap";

/** 系统频道定义 */
export const enum CHANNEL {
    /** 默认全服频道 */
    DEFAULT
}
/**
 * 管理所有agent实例
 */
export class AgentPool {
    private static _ins: AgentPool;
    public static get inst(): AgentPool {
        return this._ins ? this._ins : this._ins = new AgentPool();
    }

    /** 所有agent缓存池 */
    private _agents: HashMap<number, Agent> = new HashMap();
    /** 频道,每个频道对应一个HashMap */
    private _channels: HashMap<number, HashMap<number, Agent>> = new HashMap();
    constructor() {

    }

    /** 添加agent */
    public addAgent(id: number, agent: Agent) {
        if (this._agents.has(id)) {
            console.error(' agent already exists : ' + id);
            return;
        }
        this._agents.add(id, agent);
        this.addToChannel(CHANNEL.DEFAULT, id, agent);
    }

    /** 删除agent */
    public removeAgent(id: number, removeFromChannel: boolean = true) {
        if (this._agents.has(id)) {
            let agent = this._agents.get(id);
            agent.destroy();
            this._agents.remove(id);
            if (removeFromChannel) {
                this.removeAgentFromChannel(CHANNEL.DEFAULT, id);
            }
        }
    }

    /** 清理频道内的agent引用 */
    private removeAgentFromChannel(channel: CHANNEL, id: number) {
        if (this._channels.has(channel)) {
            let agentHash = this._channels.get(channel);
            if (agentHash.has(id)) {
                agentHash.remove(id);
            }
        }
    }

    /** 踢出频道 */
    private kickFromChannel(channel: CHANNEL, id: number, isClear: boolean = false) {
        this.removeAgentFromChannel(channel, id);
        if (isClear) {
            this.removeAgent(id, false);
        }
    }

    /** 加入频道 */
    private addToChannel(channel: CHANNEL, id: number, agent: Agent) {
        if (!this._channels.has(channel)) {
            this._channels.add(channel, new HashMap());
        }

        let agentHash = this._channels.get(channel);
        if (agentHash.has(id)) {
            console.error(' agent already exists : ' + id + ', channel:' + channel);
            return;
        }
        agentHash.add(id, agent);
    }

    /** 是否存在agent */
    public hasAgent(id: number): boolean {
        return this._agents.has(id);
    }

    /** 获取agent */
    public getAgent(uid: number): Agent {
        return this._agents.get(uid);
    }

    /** 按照频道广播 */
    public broadcast(channel: CHANNEL, message: any) {
        if (this._channels.has(channel)) {
            let agentHash = this._channels.get(channel);
            agentHash.forEach((id: number, agent: Agent) => {
                // todo 发送数据
            });
        } else {
            console.error('频道不存在：' + channel);
        }
    }
}