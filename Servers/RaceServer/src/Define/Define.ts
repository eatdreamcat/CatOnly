/** 服务端返回的状态码，自定义的code必须在3000 - 4999之间 */
export const enum CODE  {
    /** 限制登陆 */
    LOGIN_LIMIT = 3000,
    /** 重复登陆，是否踢人 */
    DUPLICATE_LOGIN_KICK,
    /** 被踢下线 */
    BEKICKED
}

/** uid基数 */
export const UID_BASE: number = 50000;

/**
 * 匹配的类型
 */
export enum MatchType {
    /** 1v1战斗 */
    Fight_1V1,
}

/** 表名 */
export const Collections = {
    /** 玩家基础数据 */
    BaseInfo: 'player_baseinfo',
    /** 战斗数据 */
    FightData: 'player_fight_data'
}

/** 排位初始分 */
export const BaseRank = 1200;