/**
 * 连接mongoDB用
 */
import mongodb from 'mongodb';
let url = "mongodb://localhost:27017/";
let user = "chichi";
let password = "mango7";
let dbName = "blued";
/** 排序类型 */
export const enum SORT_TYPE {
    /** 降序 */
    DESC = -1,
    /** 升序 */
    ASC = 1
}
export class Mongo {
    private static _ins: Mongo;
    public static get inst(): Mongo {
        return this._ins ? this._ins : this._ins = new Mongo();
    }

    constructor() {
        
    }

    /** 连接db */
    private _connect():Promise<mongodb.MongoClient> {
        return new Promise(function(resolve, reject) {
            mongodb.MongoClient.connect(url, {
                useNewUrlParser: false,
                auth: {
                    user: user,
                    password: password
                }
            }, function(err, db){
                if (err) {
                    console.error(' connect mongo error: ' + err);
                    reject(err);
                } else {
                    console.log(' connect to mongo success !');
                    resolve(db);
                }
            });
        });
    }

    /**
     * 插入数据
     * @param collection 表名 
     * @param data 数据集合
     */
    public insert(collection: string, data: Array<any>): Promise<mongodb.InsertWriteOpResult> {
        let self = this;
        return new Promise(function(resolve, reject) {
            self._connect().then((dbClient) => {
                dbClient.db(dbName).collection(collection).insertMany(data, function(err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                    dbClient.close();
                });
            }).catch((reason) => {
                reject(reason);
            })
        });
    }

    /**
     * 查询数据
     * @param collection 表名 
     * @param where 查询条件，例如{“uid”: 2000}
     * @param sort 按字段排序, {sortType: 1 升序，-1 降序}
     */
    public find(collection: string, where = {}, sort = {}): Promise<any[]> {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).find(where).sort(sort).toArray();
        });
    }

    /**
     * 查询记录条数
     * @param collection 
     */
    public count(collection: string): Promise<number> {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).find().count();
        });
    }

    /**
     * 更新数据
     * @param collection 表名
     * @param updateVal 更新的数据集合
     * @param where 查询条件
     */
    public update(collection: string, updateVal: {}, where = {}): Promise<mongodb.UpdateWriteOpResult> {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).updateMany(where, {$set: updateVal});
        });
    }

    /** 删除数据 */
    public delete(collection: string, where = {}): Promise<mongodb.DeleteWriteOpResultObject> {
        return this._connect().then((dbClient) => {
            return dbClient.db(dbName).collection(collection).deleteMany(where);
        });
    }
}