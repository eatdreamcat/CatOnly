import express from 'express';
import http from 'http';
import https from 'https';
import ws from 'ws';
import { Mongo } from './src/Mongo/mongo';
import { AgentPool } from './src/Controller/AgentPool';
import { Agent } from './src/agent';
import { Player } from './src/Game/player';
import { WordCheck } from './src/Utils/WordCheck';
import { c2s } from './src/proto/proto';
import { UID_BASE } from './src/Define/Define';

/** 通信消息的格式 */
interface Message {
    method: string
    data: any
}

let app = express();
let server = new http.Server(app);
WordCheck.inst.loadSentiveWord('res/table/forbids.ini');

server.listen(8001, function () {
    console.log(' server listen on 8001!');
});

let ws_server = new ws.Server({
    port: 8002
}, function () {
    console.log(' ================ ws start port:8002 ================== ');
});

/** 限制登陆连接 */
function originIsAllowed() {
    return true;
}

ws_server.on('connection', function (ws: WebSocket) {
    
    if (!originIsAllowed()) {
        ws.close(4000, 'not allow to connect!');
        return;
    } else {
        console.log(' connect success ！');
        ws.send(JSON.stringify({ data: "connected" }));
    }

    function onRequest(message: Message) {
        if (!AgentPool.inst.hasAgent(message.data['uid'])) {
            AgentPool.inst.addAgent(message.data['uid'], new Agent(ws, message.data['uid']));
        }
        let agent = AgentPool.inst.getAgent(message.data['uid']);
        if (agent[message.method] && typeof agent[message.method] == 'function') {
            agent[message.method](message.data);
        } else {
            console.error(' 接口不存在：' + message.method);
        }
    }


    ws.onmessage = function (e: MessageEvent) {
        console.log(' ================== onmessage =================== ');

        let message: Message = JSON.parse(e.data) as Message;

        /** 如果是登陆操作，判断一下是不是平台，是不是有账号，没有就直接注册一个 */
        if (message.method == c2s.login) {
            if (message.data['openid'] && message.data['openid'].length) {
                // 来自平台
                Mongo.inst.find('player_baseinfo', {'openid': message.data['openid']}).then((arrRes) => {
                    if (arrRes.length == 0) {
                        // 没有注册过
                        Mongo.inst.count('player_baseinfo').then((totalCount) => {
                            message.data['uid'] = totalCount + UID_BASE;
                            Mongo.inst.insert('player_baseinfo', [{
                                uid: message.data['uid'],
                                openid: message.data['openid']
                            }]).then(()=>{
                                console.log('注册成功');
                                onRequest(message);
                            }).catch((reason)=>{
                                console.error(' 用户注册失败:' + reason);
                            });
                        }).catch((reason) => {
                            console.error(' 查寻用户总数失败:' + reason);
                        });
                    } else {
                        // 注册过的用户
                        message.data['uid'] = arrRes[0]['uid'];
                        onRequest(message);
                    }
                }).catch((reason) => {
                    console.error(' 数据查找失败：' + reason);
                });
            } else {
                // 没有传openid
                if (message.data['uid']) {
                    // TODO 判断一下用户在不在
                } else {
                    // TODO 注册
                }
            }
        } else {
            onRequest(message);
        }

        
    }

    ws.onclose = function (e: CloseEvent) {
        console.log(' ================== onclose ==================== ');
        console.log(e);
    }

    ws.onerror = function (e: Event) {
        console.log(' ================== onerror ==================== ');
        console.log(e);
    }



});



/***   一些http请求 */
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:7456');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials','true');
    next();
};
app.use(allowCrossDomain);
// 获取逻辑服地址
app.get('/getLogicServer', function (req, res) {
    res.send(JSON.stringify({
        url: 'ws://127.0.0.1:8002'
    }));
 });