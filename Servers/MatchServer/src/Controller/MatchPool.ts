import { Agent } from "../agent";
import { HashMap } from "../Utils/HashMap";
import { gMongo } from "../Mongo/mongo";
import { Collections, MatchType, BaseRank } from "../Define/Define";
import { gTimer } from "./TimerManager";

/** 战斗数据 */
export interface FightData {
    uid: number
    /** 排位分 */
    rank: any // { racetype： score}
    /** 连胜数 */
    winStreak: any // { racetype: count}
    /** 最高连胜数 */
    maxWinStreak: any // { racetype: maxcount}
    /** 胜率 */
    winRate: any // {racetype: rate}
    /** 招牌英雄 */
    signatureHero: Array<number>
}

/** 匹配的数据 */
interface MatchData {
    matchType: MatchType,
    friends: FightDataExt[],
    enemys: FightDataExt[]
}

/**
 * 需要返回给客户端的数据
 */
export interface FightDataExt extends FightData {
    /** 昵称 */
    nick: string,
    /** 头像 */
    avatarUrl: string,
    
}

/** 红蓝方 */
export enum RaceSide {
    Left,
    Right
}

/**
 * 所有匹配池
 */
class MatchPool {
    private static _ins: MatchPool;
    public static get inst(): MatchPool {
        return this._ins ? this._ins : this._ins = new MatchPool();
    }

    /** 匹配池 */
    private matchPool: HashMap<MatchType, HashMap<number, Agent>> = new HashMap();
    
    constructor() {
        gTimer.setInterval(500, this, this.match1V1);
    }

    /** 每半秒执行一次匹配 */
    private match1V1(dt) {
        let matchPool = this.matchPool.get(MatchType.Fight_1V1);
        if (!matchPool) return;
        matchPool.sort((a, b) => {
            return b.value.matchLastTime - a.value.matchLastTime;
        });

        /**
         * 匹配成功的玩家
         * [{left: [], right: []}, {left: [], right: []} ...]
         */
        let matched: Array<{left: number[], right: number[]}> = [];
        for (let uid of matchPool.keys) {
            let player1 = matchPool.get(uid);
            if (!player1.isMatching()) continue;

            for (let uidSub of matchPool.keys) {
                if (uidSub == uid) continue;
                let player2 = matchPool.get(uidSub);
                if (!player2.isMatching()) continue;

                if (Math.abs(player1.FightData.rank - player2.FightData.rank) < 500) {
                    if (Math.random() > 0.5) {
                        matched.push({
                            left: [uidSub],
                            right: [uid]
                        });
                    } else {
                        matched.push({
                            right: [uidSub],
                            left: [uid]
                        })
                    }
                    player1.stopMatch();
                    player2.stopMatch();
                    break;
                }
            }
        }

        for (let matchedPlayer of matched) {
            let leftFightData: FightDataExt[] = [];
            let rightFightData: FightDataExt[] = [];

            for (let uid of matchedPlayer.left) {
                leftFightData.push(matchPool.get(uid).FightData);
            }

            for (let uid of matchedPlayer.right) {
                rightFightData.push(matchPool.get(uid).FightData);
            }

            for (let uid of matchedPlayer.left) {
                matchPool.get(uid).sendMatchSuccess(MatchType.Fight_1V1, leftFightData, rightFightData);
            }

            for (let uid of matchedPlayer.right) {
                matchPool.get(uid).sendMatchSuccess(MatchType.Fight_1V1, rightFightData, leftFightData);
            }
            /** 匹配对象的移除需要客户端收到匹配成功后手动发送cancel指令来移除 */
        }

    }

    /** 是否在匹配池中 */
    public isMatching(matchType: MatchType, uid: number) {
        let pool = this.matchPool.get(matchType);
        return pool && pool.has(uid);
    }

    /** 获取匹配池中的agent */
    public getMatchAgent(matchType: MatchType, uid: number): Agent {
        let pool = this.matchPool.get(matchType);
        if (!pool) return null;
        return pool.get(uid);
    }

    /** 从匹配池中移除 */
    public removeFromPool(matchType: MatchType, uid: number) {
        console.log('移除匹配池:' + matchType + ',type:' + MatchType[matchType] + ', uid:' + uid);
        let pool = this.matchPool.get(matchType);
        if (pool && pool.has(uid)) {
            pool.remove(uid);
        }
        console.log(pool);
    }

    /** 将玩家战斗数据加入匹配池 */
    public addToMatchPool(matchType: MatchType, agent: Agent, callback: Function) {

        let uid = agent.uid;
        let matchPool = this.matchPool.get(matchType);
        if (!matchPool) {
            matchPool = new HashMap();
            this.matchPool.add(matchType, matchPool);
        }
        // 查找玩家战斗数据
        gMongo.find(Collections.FightData, { uid : uid }).then((arrRes) => {
            if (arrRes.length <= 0) {
                // 玩家还没有战斗过，没有记录
                let fightData: FightData = {
                    uid: uid,
                    rank: BaseRank,
                    winRate: {
                        [MatchType.Fight_1V1]: 0
                    },
                    maxWinStreak: {
                        [MatchType.Fight_1V1]: 0
                    },
                    winStreak: {
                        [MatchType.Fight_1V1]: 0
                    },
                    signatureHero: []
                }
                gMongo.insert(Collections.FightData, [fightData]).then(() => {
                    
                    agent.setFightData(fightData, (success, err) => {
                        callback(success, err);
                        if (success) {
                            matchPool.add(agent.uid, agent);
                        }
                    });
                    
                }).catch((reason) => {
                    console.error('初始化战斗数据失败:' + reason);
                    callback(false, reason);
                });
            } else {
                let data = arrRes[0] as FightData;
                agent.setFightData(data, (success, err) => {
                    callback(success, err);
                    if (success) {
                        matchPool.add(agent.uid, agent);
                    }
                });
            }
        }).catch((reason) => {
            console.error(' 查询战斗数据失败:' + reason);
            callback(false, reason);
        });
    }
}
/** 匹配池 */
export const gMatchPool = MatchPool.inst;