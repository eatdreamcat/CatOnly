import { gMongo } from '../Mongo/mongo';
import { FightDataExt } from '../Controller/MatchPool';
/**
 * 玩家数据实例
 */
export class Player {

    /** 玩家uid */
    public readonly uid: number = 0;
    /** 战斗数据 */
    private fightData: FightDataExt = null;
    constructor(uid: number) {
        this.uid = uid;
    }

    setFightData(fightData: FightDataExt) {
        this.fightData = fightData;
    }

    getFightData() {
        return this.fightData;
    }
    


}