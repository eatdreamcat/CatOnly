import { Player } from "./Game/player";
import { MatchType, Collections, CODE } from "./Define/Define";
import { gMatchPool, FightData, FightDataExt } from "./Controller/MatchPool";
import { gMongo, player_baseinfo } from "./Mongo/mongo";

/** 匹配的数据 */
interface MatchData {
    matchType: MatchType,
    friends: FightDataExt[],
    enemys: FightDataExt[]
}


/**
 * 前后端通信的实例
 * 每一个玩家连接都对应一个agent对象
 */
export class Agent {
    /** ws 引用 */
    private client: WebSocket = null;

    private player: Player = null;
    /** 是否可以开始匹配 */
    private _isMatching: boolean = false;
    /** 当前匹配的类型 */
    private _curMatchType: MatchType = null;
    /** 累计匹配时间 */
    public matchLastTime: number = 0;
    /** 累计匹配次数 */
    public matchTimes: number = 0;

    /** 匹配到的玩家们 */
    private matchedUids: number[] = [];

    constructor(ws: WebSocket, uid: number) {
        this.client = ws;
        this.player = new Player(uid);
        this.client.onclose = this.onClose.bind(this);
        this.client.onerror = this.onError.bind(this);
    }

    public get uid() {
        return this.player.uid;
    }

    /** 设置战斗数据 */
    public setFightData(fightData: FightData, callback: { (success: boolean, err: string): void }) {
        gMongo.find(Collections.BaseInfo, { uid: this.uid }).then((arrRes) => {
            if (arrRes.length <= 0) {
                callback(false, '未查询到用户数据');
            } else {

                let data = arrRes[0] as player_baseinfo;
                let fightDataExt: FightDataExt = {
                    uid: data.uid,
                    nick: data.nick,
                    avatarUrl: data.avatarUrl,
                    rank: fightData.rank,
                    winRate: fightData.winRate,
                    winStreak: fightData.winStreak,
                    maxWinStreak: fightData.maxWinStreak,
                    signatureHero: fightData.signatureHero

                }
                this.player.setFightData(fightDataExt);
                callback(true, 'OK');
            }
        }).catch((reason) => {
            console.error(reason);
            callback(false, '查询用户失败');
        });
    }

    /** 获取战斗数据 */
    public get FightData() {
        return this.player.getFightData();
    }

    /** 开始匹配 */
    startMatch(ws, data: { uid: number, matchType: MatchType }) {
        console.log(' ------------ start match ---------------');
        console.log(data);
        this._isMatching = true;
        this._curMatchType = data.matchType;
        gMatchPool.addToMatchPool(this._curMatchType, this, (success: boolean, errMsg: string = '') => {
            this.client.send(JSON.stringify({
                method: 'startMatch',
                success: success,
                err: errMsg
            }));
        });
    }

    /** 取消匹配 */
    cancelMatch() {
        if (!this.isMatching || !this._curMatchType) {
            console.error(' 未处于匹配态!');
            return;
        }
        this._isMatching = false;
        this.client.send(JSON.stringify({
            method: 'cancelMatch',
            success: true,
            err: '取消匹配成功'
        }));
        gMatchPool.removeFromPool(this._curMatchType, this.uid);
    }

    /** 匹配被迫断开 */
    forceCancel() {
        if (this.isMatching()) {
            console.error('正在匹配中...!');
            return;
        }

        this.client.send(JSON.stringify({
            method: 'forceCancel',
            success: true,
            err: '有玩家断开了',
            data: {
                matchType: this._curMatchType
            }
        }));

        // 重新开始匹配
        this._isMatching = true;
    }

    /** 通知客户端匹配成功 */
    sendMatchSuccess(matchType: MatchType, friends: FightDataExt[], enemys: FightDataExt[]) {
        this._isMatching = false;
        for (let friend of friends) {
            if (friend.uid != this.uid) {
                this.matchedUids.push(friend.uid);
            }
        }

        for (let enemy of enemys) {
            this.matchedUids.push(enemy.uid);
        }

        let matchData: MatchData = {
            matchType: matchType,
            friends: friends,
            enemys: enemys
        }

        this.client.send(JSON.stringify({
            method: 'onMatchSuccess',
            success: true,
            data: matchData,
            err: '匹配玩家成功'
        }));
    }

    /** 是否是匹配状态 */
    public isMatching() {
        return this._isMatching;
    }

    /** 停止匹配 */
    public stopMatch() {
        this._isMatching = false;
    }

    /** 清理agent */
    onClose(e: CloseEvent) {
        console.log(' onClose -----------------');
        console.log(e);
        // 需要处理一下匹配成功时断开的情况
        for (let uid of this.matchedUids) {
            //通知匹配到的玩家，本次匹配失效了
            let agent = gMatchPool.getMatchAgent(this._curMatchType, uid);
            console.log(agent);
            if (agent) {
                agent.forceCancel();
            }
        }
        gMatchPool.removeFromPool(this._curMatchType, this.uid);
    }

    onError(e: Event) {
        console.log('on error -----');
        console.log(e);
        // 需要处理一下匹配成功时断开的情况
        for (let uid of this.matchedUids) {
            //通知匹配到的玩家，本次匹配失效了
            let agent = gMatchPool.getMatchAgent(this._curMatchType, uid);
            if (agent) {
                agent.forceCancel();
            }
        }
        gMatchPool.removeFromPool(this._curMatchType, this.uid);
    }

}