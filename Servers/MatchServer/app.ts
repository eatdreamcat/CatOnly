import express from 'express';
import http from 'http';
import ws from 'ws';
import { gMongo } from './src/Mongo/mongo';
import { gMatchPool } from './src/Controller/MatchPool';
import { Agent } from './src/agent';
import { WordCheck } from './src/Utils/WordCheck';

/** 通信消息的格式 */
interface Message {
    method: string
    data: any
}
/**
 * 匹配服务器
 * 玩家开始匹配，把玩家的战斗数据加入匹配池，玩家取消匹配或者匹配成功移除匹配池
 * 根据玩家分数跟匹配时间
 * 匹配时间越长，分数范围越大
 * 返回匹配结果
 */
let app = express();
let server = new http.Server(app);
WordCheck.inst.loadSentiveWord('res/table/forbids.ini');

server.listen(9001, function () {
    console.log(' match server listen on 9001!');
});

let ws_server = new ws.Server({
    port: 9002
}, function () {
    console.log(' ================ match ws start port:9002 ================== ');
});

/** 限制登陆连接 */
function originIsAllowed() {
    return true;
}

ws_server.on('connection', function (ws: WebSocket) {

    if (!originIsAllowed()) {
        ws.close(4000, 'not allow to connect!');
        return;
    } else {
        console.log(' connect success ！');
        ws.send(JSON.stringify({ data: "connected" }));
    }

    function onRequest(message: Message) {
        // 心跳
        if (message.method == 'heartbeat') {
            ws.send(JSON.stringify({
                method: message.method,
                success: true,
                err: 'OK'
            }));
            return;
        }

        console.log(message);
        if (!message.data['matchType'] || !message.data['uid']) {
            ws.send(JSON.stringify({
                method: message.method,
                success: false,
                err: '缺少必要参数'
            }));
            return;
        }

        let agent = gMatchPool.getMatchAgent(message.data['matchType'], message.data['uid']);
        if (!agent) {
            agent = new Agent(ws, message.data['uid']);
        }
        if (agent[message.method] && typeof agent[message.method] == 'function') {
            agent[message.method](ws, message.data);
        } else {
            console.error(' 接口不存在：' + message.method);
            ws.send(JSON.stringify({
                method: message.method,
                success: false,
                err: '接口不存在'
            }));
        }
    }


    ws.onmessage = function (e: MessageEvent) {
        console.log(' ================== onmessage =================== ');

        let message: Message = JSON.parse(e.data) as Message;
        onRequest(message);
    }

    ws.onclose = function (e: CloseEvent) {
        console.log(' ================== onclose ==================== ');
        console.log(e);
    }

    ws.onerror = function (e: Event) {
        console.log(' ================== onerror ==================== ');
        console.log(e);
    }



});
