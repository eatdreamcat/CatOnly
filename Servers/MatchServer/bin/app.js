"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const ws_1 = __importDefault(require("ws"));
const MatchPool_1 = require("./src/Controller/MatchPool");
const agent_1 = require("./src/agent");
const WordCheck_1 = require("./src/Utils/WordCheck");
/**
 * 匹配服务器
 * 玩家开始匹配，把玩家的战斗数据加入匹配池，玩家取消匹配或者匹配成功移除匹配池
 * 根据玩家分数跟匹配时间
 * 匹配时间越长，分数范围越大
 * 返回匹配结果
 */
let app = express_1.default();
let server = new http_1.default.Server(app);
WordCheck_1.WordCheck.inst.loadSentiveWord('res/table/forbids.ini');
server.listen(9001, function () {
    console.log(' match server listen on 9001!');
});
let ws_server = new ws_1.default.Server({
    port: 9002
}, function () {
    console.log(' ================ match ws start port:9002 ================== ');
});
/** 限制登陆连接 */
function originIsAllowed() {
    return true;
}
ws_server.on('connection', function (ws) {
    if (!originIsAllowed()) {
        ws.close(4000, 'not allow to connect!');
        return;
    }
    else {
        console.log(' connect success ！');
        ws.send(JSON.stringify({ data: "connected" }));
    }
    function onRequest(message) {
        // 心跳
        if (message.method == 'heartbeat') {
            ws.send(JSON.stringify({
                method: message.method,
                success: true,
                err: 'OK'
            }));
            return;
        }
        console.log(message);
        if (!message.data['matchType'] || !message.data['uid']) {
            ws.send(JSON.stringify({
                method: message.method,
                success: false,
                err: '缺少必要参数'
            }));
            return;
        }
        let agent = MatchPool_1.gMatchPool.getMatchAgent(message.data['matchType'], message.data['uid']);
        if (!agent) {
            agent = new agent_1.Agent(ws, message.data['uid']);
        }
        if (agent[message.method] && typeof agent[message.method] == 'function') {
            agent[message.method](ws, message.data);
        }
        else {
            console.error(' 接口不存在：' + message.method);
            ws.send(JSON.stringify({
                method: message.method,
                success: false,
                err: '接口不存在'
            }));
        }
    }
    ws.onmessage = function (e) {
        console.log(' ================== onmessage =================== ');
        let message = JSON.parse(e.data);
        onRequest(message);
    };
    ws.onclose = function (e) {
        console.log(' ================== onclose ==================== ');
        console.log(e);
    };
    ws.onerror = function (e) {
        console.log(' ================== onerror ==================== ');
        console.log(e);
    };
});
//# sourceMappingURL=app.js.map