"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** uid基数 */
exports.UID_BASE = 50000;
/**
 * 匹配的类型
 */
var MatchType;
(function (MatchType) {
    /** 1v1战斗 */
    MatchType[MatchType["Fight_1V1"] = 101] = "Fight_1V1";
})(MatchType = exports.MatchType || (exports.MatchType = {}));
/** 表名 */
exports.Collections = {
    /** 玩家基础数据 */
    BaseInfo: 'player_baseinfo',
    /** 战斗数据 */
    FightData: 'player_fight_data'
};
/** 排位初始分 */
exports.BaseRank = 1200;
//# sourceMappingURL=Define.js.map