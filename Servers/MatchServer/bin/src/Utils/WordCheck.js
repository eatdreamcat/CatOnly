"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
/** 字符的类型 */
var WordType;
(function (WordType) {
    WordType[WordType["Number"] = 0] = "Number";
    WordType[WordType["Letter"] = 1] = "Letter";
    WordType[WordType["Chinese"] = 2] = "Chinese";
    WordType[WordType["Other"] = 3] = "Other";
    WordType[WordType["Default"] = 4] = "Default";
})(WordType || (WordType = {}));
/**
 * 屏蔽关键字
 */
class WordCheck {
    constructor() {
        /** 需要屏蔽的词DFA树 */
        this._dfaMapRoot = {
            word: 'root',
            children: {},
            isEnd: false
        };
        /** 当前查找的字符的类型 */
        this._currentFindType = WordType.Default;
        /** 目标字符的类型 */
        this._targetFindType = WordType.Default;
        /** 需要过滤的字符index集合 */
        this._filterArr = [];
    }
    static get inst() {
        return this._ins ? this._ins : this._ins = new WordCheck();
    }
    /**
     * 加载敏感字符
     * 遍历关键词list：['aab', 'ab', 'b', 'bbc']
     * 每个词从根节点开始，如果首字存在，进入chidren找下一个字，直到词完结
     * 不存在则直接添加
     * 生成 {
     *        a : {
     *            a : {
     *                 b
     *               },
     *            b : {
     *                }
     *         },
     *        b : {
     *             b: {}
     *             b: {
     *               c : {}
     *             }
     *            }
     *      }
     * */
    loadSentiveWord(url) {
        try {
            let wordList = [];
            let words = fs_1.default.readFileSync(url);
            let separators = ["\r\n", "\r", "\n"];
            for (let sep of separators) {
                wordList = words.toString().split(sep);
                if (wordList.length > 1) { //拆分成功
                    break;
                }
            }
            this.toDFATree(wordList);
        }
        catch (error) {
            console.error(' 加载字库失败:' + error);
        }
    }
    /** 转化成树 */
    toDFATree(wordList) {
        for (let word of wordList) {
            this.setNode(word.trim().split(''), this._dfaMapRoot);
        }
    }
    /**
     * 设置节点信息
     * 把每个词生成DFA树的结构
     * */
    setNode(wordArr, currenyNode) {
        if (wordArr.length <= 0) {
            currenyNode.isEnd = true;
            return;
        }
        let singleWord = wordArr.shift();
        while (!singleWord.length) {
            if (wordArr.length <= 0) {
                currenyNode.isEnd = true;
                return;
            }
            singleWord = wordArr.shift();
        }
        /**
         * 当前节点不存在这个字，直接添加为子节点
         */
        if (!currenyNode.children[singleWord]) {
            currenyNode.children[singleWord] = {
                word: singleWord,
                children: {},
                isEnd: false
            };
        }
        this.setNode(wordArr, currenyNode.children[singleWord]);
    }
    /** 返回字符类型 */
    checkWordType(word) {
        if (/[0-9]/.test(word)) {
            // 数字
            return WordType.Number;
        }
        else if (/[A-Z]/.test(word) || /[a-z]/.test(word)) {
            // 字母
            return WordType.Letter;
        }
        else if (escape(word).indexOf('%u') >= 0) {
            // 汉字
            return WordType.Chinese;
        }
        else {
            // 其他符号
            return WordType.Other;
        }
    }
    /**
     * 敏感字检测
     * @param wordArr 需要检测的字符
     * @param replaceString 替换的字符
     * @param strickMode 检测模式
     * @param isFitler 是否过滤，不过滤的时候直接返回boolean
     */
    filterSentiveWord(wordArr, replaceString = '*', strickMode = true) {
        this._filterArr = [];
        this._currentFindType = WordType.Default;
        this._targetFindType = WordType.Default;
        /** 敏感词检测 */
        let length = wordArr.length;
        /**
         * 从DFA树的头开始，直到某一个end节点，说明敏感词匹配成功
         * 否则一直查找end，查找不到就说明不是敏感词
         */
        for (let i = 0; i < length; ++i) {
            // 已经屏蔽
            if (this._filterArr.indexOf(i) >= 0) {
                continue;
            }
            // 当前起始字
            let curFirstWord = wordArr[i];
            this._targetFindType = this.checkWordType(curFirstWord);
            // 数字跟符号构不成敏感词,直接下一个,严格模式下不跳过检测
            if ((this._targetFindType == WordType.Other || this._targetFindType == WordType.Number) && !strickMode) {
                continue;
            }
            //根据当前首个字，开始匹配敏感词,从根节点开始匹配
            let curFindNode = this._dfaMapRoot; // 查找的当前节点
            // 当前查找的字的所有index集合，如果构成敏感词，则记录到屏蔽下标集合，否则清空，继续匹配下一个
            let sentiveFilterTempArr = [];
            for (let j = i; j < length; ++j) {
                // 匹配到结束，说明是敏感词
                if (curFindNode.isEnd) {
                    // 直接跳到该词的最后一个下标，之前的全记录在屏蔽下标集合了
                    if (!strickMode) {
                        i = j - 1;
                    }
                    break;
                }
                let findWord = wordArr[j];
                this._currentFindType = this.checkWordType(findWord);
                // 可能存在敏感词, 继续下一个字和节点匹配
                if (curFindNode.children[findWord]) {
                    curFindNode = curFindNode.children[findWord];
                    sentiveFilterTempArr.push(j); // 记录到可能的集合
                    continue;
                }
                else {
                    // 当前查找的字符类型跟前一个字符类型不一样
                    if (this._currentFindType != this._targetFindType) {
                        if (strickMode) {
                            /**
                             *  严格模式下，直接跳过不一致类型的字符，继续查找
                             */
                            continue;
                        }
                        else {
                            // 非严格模式，直接判定当前查找的字符不会构成敏感词，继续查找下一个词
                            sentiveFilterTempArr = [];
                            break;
                        }
                    }
                    else {
                        // 相同类型也无法构成敏感字
                        sentiveFilterTempArr = [];
                        break;
                    }
                }
            }
            /** 检测已经完成，但是组词没成功，说明不算敏感词 */
            if (curFindNode.isEnd) {
                this._filterArr = this._filterArr.concat(sentiveFilterTempArr);
            }
        }
        for (let index of this._filterArr) {
            if (wordArr[index]) {
                wordArr[index] = replaceString;
            }
            else {
                console.warn(' 算法有bug吧, 下标:' + index + '的字符串不存在');
            }
        }
        return wordArr.join('');
    }
}
exports.WordCheck = WordCheck;
//# sourceMappingURL=WordCheck.js.map