"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HashMap_1 = require("../Utils/HashMap");
/**
 * timer管理
 */
class TimerManager {
    constructor() {
        /** 是否暂停 */
        this.isStop = false;
        /** 上次调用的时间 */
        this.lastCallTime = 0;
        /** 主update */
        this.mainTimer = null;
        /** update间隔 毫秒*/
        this.DELTTIME = 1000 / 60;
        /** 注册的update方法 */
        this.intervalHash = new HashMap_1.HashMap();
        /** 注册的总数，用来作为ID */
        this.intervalTotalCount = 0;
        console.log(' Timer init!');
        this.isStop = false;
        this.lastCallTime = Date.now();
        this.mainTimer = setTimeout(this.update.bind(this), this.DELTTIME);
    }
    static get inst() {
        return this._ins ? this._ins : this._ins = new TimerManager();
    }
    /**
     * 添加到interval，handler默认第一个参数都是dt
     * @param delay 时间间隔, 毫秒
     * @param caller 调用的this环境
     * @param handler 方法
     * @param args 参数列表
     * @param repeatTimes 重复次数，小于0代表不限制，默认-1
     * @param isOnce 只执行一次,默认false
     */
    setInterval(delay, caller, handler, args = [], repeatTimes = -1, isOnce = false) {
        for (let key of this.intervalHash.keys) {
            let timer = this.intervalHash.get(key);
            if (timer && timer.caller == caller && timer.handler == handler) {
                console.warn(' timer already added: ' + key);
                return key;
            }
        }
        if (delay < this.DELTTIME) {
            console.error(' 时间间隔太短！不得低于一帧的时间！');
            delay = this.DELTTIME;
        }
        ++this.intervalTotalCount;
        this.intervalHash.add(this.intervalTotalCount, {
            caller: caller,
            handler: handler,
            delay: delay,
            lastCallTime: Date.now(),
            curTimes: 0,
            args: args,
            totalTimes: repeatTimes,
            isOnce: isOnce
        });
        return this.intervalTotalCount;
    }
    /** 主要update循环 */
    update() {
        if (this.isStop)
            return;
        let now = Date.now();
        let dt = now - this.lastCallTime;
        let deleteHashKey = [];
        // 处理各种handler的执行
        for (let key of this.intervalHash.keys) {
            let timer = this.intervalHash.get(key);
            if (!timer) {
                deleteHashKey.push(key);
                console.warn(' timer invaild! ID: ' + key);
                continue;
            }
            if (timer.caller && timer.handler) {
                if (timer.isOnce && timer.curTimes >= 1) {
                    deleteHashKey.push(key);
                    continue;
                }
                if (timer.totalTimes >= 0 && timer.curTimes >= timer.totalTimes) {
                    deleteHashKey.push(key);
                    continue;
                }
                /** 未到达时间间隔 */
                if (timer.lastCallTime + timer.delay > now)
                    continue;
                let args = [now - timer.lastCallTime].concat(timer.args);
                timer.lastCallTime = now;
                timer.handler.apply(timer.caller, args);
                ++timer.curTimes;
            }
            else {
                deleteHashKey.push(key);
                continue;
            }
        }
        //清理需要清楚的updateHandler
        for (let key of deleteHashKey) {
            this.intervalHash.remove(key);
        }
        this.lastCallTime = now;
        //实际时间间隔大于设定的间隔，提前进入下一个update
        if (dt > this.DELTTIME) {
            // 间隔时间超过两倍的delt
            while (2 * this.DELTTIME - dt <= 0) {
                this.update();
                dt -= this.DELTTIME;
            }
            this.mainTimer = setTimeout(this.update.bind(this), 2 * this.DELTTIME - dt);
        }
        else {
            // 实际用适比较少，就多等待
            this.mainTimer = setTimeout(this.update.bind(this), this.DELTTIME + this.DELTTIME - dt);
        }
        //this.mainTimer = setTimeout(this.update.bind(this), this.DELTTIME);
    }
}
/**
 * 调度管理器
 */
exports.gTimer = TimerManager.inst;
//# sourceMappingURL=TimerManager.js.map