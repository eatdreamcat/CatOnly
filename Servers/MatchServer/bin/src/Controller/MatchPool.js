"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HashMap_1 = require("../Utils/HashMap");
const mongo_1 = require("../Mongo/mongo");
const Define_1 = require("../Define/Define");
const TimerManager_1 = require("./TimerManager");
/** 红蓝方 */
var RaceSide;
(function (RaceSide) {
    RaceSide[RaceSide["Left"] = 0] = "Left";
    RaceSide[RaceSide["Right"] = 1] = "Right";
})(RaceSide = exports.RaceSide || (exports.RaceSide = {}));
/**
 * 所有匹配池
 */
class MatchPool {
    constructor() {
        /** 匹配池 */
        this.matchPool = new HashMap_1.HashMap();
        TimerManager_1.gTimer.setInterval(500, this, this.match1V1);
    }
    static get inst() {
        return this._ins ? this._ins : this._ins = new MatchPool();
    }
    /** 每半秒执行一次匹配 */
    match1V1(dt) {
        let matchPool = this.matchPool.get(Define_1.MatchType.Fight_1V1);
        if (!matchPool)
            return;
        matchPool.sort((a, b) => {
            return b.value.matchLastTime - a.value.matchLastTime;
        });
        /**
         * 匹配成功的玩家
         * [{left: [], right: []}, {left: [], right: []} ...]
         */
        let matched = [];
        for (let uid of matchPool.keys) {
            let player1 = matchPool.get(uid);
            if (!player1.isMatching())
                continue;
            for (let uidSub of matchPool.keys) {
                if (uidSub == uid)
                    continue;
                let player2 = matchPool.get(uidSub);
                if (!player2.isMatching())
                    continue;
                if (Math.abs(player1.FightData.rank - player2.FightData.rank) < 500) {
                    if (Math.random() > 0.5) {
                        matched.push({
                            left: [uidSub],
                            right: [uid]
                        });
                    }
                    else {
                        matched.push({
                            right: [uidSub],
                            left: [uid]
                        });
                    }
                    player1.stopMatch();
                    player2.stopMatch();
                    break;
                }
            }
        }
        for (let matchedPlayer of matched) {
            let leftFightData = [];
            let rightFightData = [];
            for (let uid of matchedPlayer.left) {
                leftFightData.push(matchPool.get(uid).FightData);
            }
            for (let uid of matchedPlayer.right) {
                rightFightData.push(matchPool.get(uid).FightData);
            }
            for (let uid of matchedPlayer.left) {
                matchPool.get(uid).sendMatchSuccess(Define_1.MatchType.Fight_1V1, leftFightData, rightFightData);
            }
            for (let uid of matchedPlayer.right) {
                matchPool.get(uid).sendMatchSuccess(Define_1.MatchType.Fight_1V1, rightFightData, leftFightData);
            }
            /** 匹配对象的移除需要客户端收到匹配成功后手动发送cancel指令来移除 */
        }
    }
    /** 是否在匹配池中 */
    isMatching(matchType, uid) {
        let pool = this.matchPool.get(matchType);
        return pool && pool.has(uid);
    }
    /** 获取匹配池中的agent */
    getMatchAgent(matchType, uid) {
        let pool = this.matchPool.get(matchType);
        if (!pool)
            return null;
        return pool.get(uid);
    }
    /** 从匹配池中移除 */
    removeFromPool(matchType, uid) {
        console.log('移除匹配池:' + matchType + ',type:' + Define_1.MatchType[matchType] + ', uid:' + uid);
        let pool = this.matchPool.get(matchType);
        if (pool && pool.has(uid)) {
            pool.remove(uid);
        }
        console.log(pool);
    }
    /** 将玩家战斗数据加入匹配池 */
    addToMatchPool(matchType, agent, callback) {
        let uid = agent.uid;
        let matchPool = this.matchPool.get(matchType);
        if (!matchPool) {
            matchPool = new HashMap_1.HashMap();
            this.matchPool.add(matchType, matchPool);
        }
        // 查找玩家战斗数据
        mongo_1.gMongo.find(Define_1.Collections.FightData, { uid: uid }).then((arrRes) => {
            if (arrRes.length <= 0) {
                // 玩家还没有战斗过，没有记录
                let fightData = {
                    uid: uid,
                    rank: Define_1.BaseRank,
                    winRate: {
                        [Define_1.MatchType.Fight_1V1]: 0
                    },
                    maxWinStreak: {
                        [Define_1.MatchType.Fight_1V1]: 0
                    },
                    winStreak: {
                        [Define_1.MatchType.Fight_1V1]: 0
                    },
                    signatureHero: []
                };
                mongo_1.gMongo.insert(Define_1.Collections.FightData, [fightData]).then(() => {
                    agent.setFightData(fightData, (success, err) => {
                        callback(success, err);
                        if (success) {
                            matchPool.add(agent.uid, agent);
                        }
                    });
                }).catch((reason) => {
                    console.error('初始化战斗数据失败:' + reason);
                    callback(false, reason);
                });
            }
            else {
                let data = arrRes[0];
                agent.setFightData(data, (success, err) => {
                    callback(success, err);
                    if (success) {
                        matchPool.add(agent.uid, agent);
                    }
                });
            }
        }).catch((reason) => {
            console.error(' 查询战斗数据失败:' + reason);
            callback(false, reason);
        });
    }
}
/** 匹配池 */
exports.gMatchPool = MatchPool.inst;
//# sourceMappingURL=MatchPool.js.map