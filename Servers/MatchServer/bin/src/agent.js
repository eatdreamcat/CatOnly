"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const player_1 = require("./Game/player");
const Define_1 = require("./Define/Define");
const MatchPool_1 = require("./Controller/MatchPool");
const mongo_1 = require("./Mongo/mongo");
/**
 * 前后端通信的实例
 * 每一个玩家连接都对应一个agent对象
 */
class Agent {
    constructor(ws, uid) {
        /** ws 引用 */
        this.client = null;
        this.player = null;
        /** 是否可以开始匹配 */
        this._isMatching = false;
        /** 当前匹配的类型 */
        this._curMatchType = null;
        /** 累计匹配时间 */
        this.matchLastTime = 0;
        /** 累计匹配次数 */
        this.matchTimes = 0;
        /** 匹配到的玩家们 */
        this.matchedUids = [];
        this.client = ws;
        this.player = new player_1.Player(uid);
        this.client.onclose = this.onClose.bind(this);
        this.client.onerror = this.onError.bind(this);
    }
    get uid() {
        return this.player.uid;
    }
    /** 设置战斗数据 */
    setFightData(fightData, callback) {
        mongo_1.gMongo.find(Define_1.Collections.BaseInfo, { uid: this.uid }).then((arrRes) => {
            if (arrRes.length <= 0) {
                callback(false, '未查询到用户数据');
            }
            else {
                let data = arrRes[0];
                let fightDataExt = {
                    uid: data.uid,
                    nick: data.nick,
                    avatarUrl: data.avatarUrl,
                    rank: fightData.rank,
                    winRate: fightData.winRate,
                    winStreak: fightData.winStreak,
                    maxWinStreak: fightData.maxWinStreak,
                    signatureHero: fightData.signatureHero
                };
                this.player.setFightData(fightDataExt);
                callback(true, 'OK');
            }
        }).catch((reason) => {
            console.error(reason);
            callback(false, '查询用户失败');
        });
    }
    /** 获取战斗数据 */
    get FightData() {
        return this.player.getFightData();
    }
    /** 开始匹配 */
    startMatch(ws, data) {
        console.log(' ------------ start match ---------------');
        console.log(data);
        this._isMatching = true;
        this._curMatchType = data.matchType;
        MatchPool_1.gMatchPool.addToMatchPool(this._curMatchType, this, (success, errMsg = '') => {
            this.client.send(JSON.stringify({
                method: 'startMatch',
                success: success,
                err: errMsg
            }));
        });
    }
    /** 取消匹配 */
    cancelMatch() {
        if (!this.isMatching || !this._curMatchType) {
            console.error(' 未处于匹配态!');
            return;
        }
        this._isMatching = false;
        this.client.send(JSON.stringify({
            method: 'cancelMatch',
            success: true,
            err: '取消匹配成功'
        }));
        MatchPool_1.gMatchPool.removeFromPool(this._curMatchType, this.uid);
    }
    /** 匹配被迫断开 */
    forceCancel() {
        if (this.isMatching()) {
            console.error('正在匹配中...!');
            return;
        }
        this.client.send(JSON.stringify({
            method: 'forceCancel',
            success: true,
            err: '有玩家断开了',
            data: {
                matchType: this._curMatchType
            }
        }));
        // 重新开始匹配
        this._isMatching = true;
    }
    /** 通知客户端匹配成功 */
    sendMatchSuccess(matchType, friends, enemys) {
        this._isMatching = false;
        for (let friend of friends) {
            if (friend.uid != this.uid) {
                this.matchedUids.push(friend.uid);
            }
        }
        for (let enemy of enemys) {
            this.matchedUids.push(enemy.uid);
        }
        let matchData = {
            matchType: matchType,
            friends: friends,
            enemys: enemys
        };
        this.client.send(JSON.stringify({
            method: 'onMatchSuccess',
            success: true,
            data: matchData,
            err: '匹配玩家成功'
        }));
    }
    /** 是否是匹配状态 */
    isMatching() {
        return this._isMatching;
    }
    /** 停止匹配 */
    stopMatch() {
        this._isMatching = false;
    }
    /** 清理agent */
    onClose(e) {
        console.log(' onClose -----------------');
        console.log(e);
        // 需要处理一下匹配成功时断开的情况
        for (let uid of this.matchedUids) {
            //通知匹配到的玩家，本次匹配失效了
            let agent = MatchPool_1.gMatchPool.getMatchAgent(this._curMatchType, uid);
            console.log(agent);
            if (agent) {
                agent.forceCancel();
            }
        }
        MatchPool_1.gMatchPool.removeFromPool(this._curMatchType, this.uid);
    }
    onError(e) {
        console.log('on error -----');
        console.log(e);
        // 需要处理一下匹配成功时断开的情况
        for (let uid of this.matchedUids) {
            //通知匹配到的玩家，本次匹配失效了
            let agent = MatchPool_1.gMatchPool.getMatchAgent(this._curMatchType, uid);
            if (agent) {
                agent.forceCancel();
            }
        }
        MatchPool_1.gMatchPool.removeFromPool(this._curMatchType, this.uid);
    }
}
exports.Agent = Agent;
//# sourceMappingURL=agent.js.map