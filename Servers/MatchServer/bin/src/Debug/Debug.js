"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Debug {
    static log(obj, ...arg) {
        console.log(obj, ...arg);
    }
    static warn(obj, ...arg) {
        console.warn(obj, ...arg);
    }
    static error(obj, ...arg) {
        console.error(obj, ...arg);
    }
    static track(obj, ...arg) {
    }
}
exports.Debug = Debug;
