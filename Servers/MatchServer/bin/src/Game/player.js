"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 玩家数据实例
 */
class Player {
    constructor(uid) {
        /** 玩家uid */
        this.uid = 0;
        /** 战斗数据 */
        this.fightData = null;
        this.uid = uid;
    }
    setFightData(fightData) {
        this.fightData = fightData;
    }
    getFightData() {
        return this.fightData;
    }
}
exports.Player = Player;
//# sourceMappingURL=player.js.map